﻿<%@ WebHandler Language="C#" Class="ProcessHandler" %>

#define TRACE

using System;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using System.Threading.Tasks;

using BejoLib;

public class BejoQuery
{
    public string TextToCorrect { get; set; }
    public string Command { get; set; }
}

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ProcessHandler : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
{
    public ProcessHandler()
    {
        if (BejoDictionary.BejoWordDictionary == null)
        {
            string dictFullPath = Helper.GetDefaultDictionaryPath();

            //1. Load dictionary
            BejoDictionary.ImportLefff(dictFullPath);
        }

        //2. Load Tagger
        POS.Initialize();
    }

    public void ProcessRequest(HttpContext context)
    {
        string input;
        using (var reader = new StreamReader(context.Request.InputStream))
        {
            input = reader.ReadToEnd();
        }

        if (string.IsNullOrEmpty(input)) return;

        BejoQuery inputQuery = new JavaScriptSerializer().Deserialize<BejoQuery>(input);

        string jsonResponse = "";
        if (inputQuery.Command == "ApplyCorrection")
        {
            BejoText btext;
            string result = "Error";
            string positionLengths = "";

            try
            {
                if (inputQuery.TextToCorrect.Length < 2000)
                {
                    btext = Correction.Execute(inputQuery.TextToCorrect, true);

                    result = btext.GetModifiedText();

                    LogQueryAsync(inputQuery.TextToCorrect, btext);

                    positionLengths = string.Join(",", btext.GetCorrectionsPositionLength());
                }
                else throw new Exception("Text too big");
            }
            catch (Exception ex)
            {
                //system logging
                System.Diagnostics.Trace.WriteLine(Environment.NewLine + ex.Message + Environment.NewLine + "==============", " Error");
            }

            jsonResponse = "{ \"CorrectedText\": \"" + EscapeStringValue(result) + "\", \"numbers\": [" + positionLengths + "] }";
        }
        else
        if (inputQuery.Command == "GetDemoDocument")
        {
            jsonResponse = "{ \"CorrectedText\": \"" + BejoLib.BejoDocumentDB.GetDocumentDemo() + "\" }";
        }

        context.Response.ContentType = "application/json"; //javascript will automatically produce a json object for the result

        context.Response.Write(jsonResponse);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public void LogQuery(string input, BejoText btext)
    {
        try
        {
            if (input != BejoDocumentDB.GetDocumentDemo()) 
                System.Diagnostics.Trace.WriteLine(Environment.NewLine + "Original text: " + input + Environment.NewLine + "Corrected text: " + btext.GetModifiedText() + Environment.NewLine + "==============", " Query");
        }
        catch //usually when log file is inaccessible
        {

        }
    }

    public void LogQueryAsync(string input, BejoText btext)
    {
        new Task(() => { LogQuery(input, btext);}).Start();
    }

    public static string EscapeStringValue(string value)
    {
        const char BACK_SLASH = '\\';
        const char SLASH = '/';
        const char DBL_QUOTE = '"';

        var output = new StringBuilder(value.Length);
        foreach (char c in value)
        {
            switch (c)
            {
                case SLASH:
                    output.AppendFormat("{0}{1}", BACK_SLASH, SLASH);
                    break;

                case BACK_SLASH:
                    output.AppendFormat("{0}{0}", BACK_SLASH);
                    break;

                case DBL_QUOTE:
                    output.AppendFormat("{0}{1}", BACK_SLASH, DBL_QUOTE);
                    break;

                default:
                    output.Append(c);
                    break;
            }
        }

        output = output.Replace("\n", "\\n");

        return output.ToString();
    }

    //public string AddColor(BejoText btext)
    //{
    //    StringBuilder coloredText = new StringBuilder();

    //    BejoWord[] taggedWords = btext.GetTaggedWords;

    //    //BejoWord[] invalidOnes = taggedWords.Where(x => x.CorrectionLength < 0).ToArray();

    //    int last = 0;

    //    //Underline
    //    foreach (var word in taggedWords)
    //    {
    //        if (word.IsCorrected)
    //        {
    //            if (!string.IsNullOrEmpty(word.CurrentBestCorrection))
    //            {
    //                coloredText.Append(btext.GetModifiedText().Substring(last, word.CorrectionPosition - last));

    //                string w = word.CurrentBestCorrection;//btext.GetModifiedText().Substring(word.CorrectionPosition, word.CorrectionLength);
    //                coloredText.Append("<FONT COLOR=\\\"red\\\">" + w + "</FONT>");

    //                last = word.CorrectionPosition + word.CorrectionLength;
    //            }
    //            //Color color;

    //            //if (word.CorrectionWords != null && word.CorrectionWords.Length > 1)
    //            //{
    //            //    color = Colors.OrangeRed; //more than one correction
    //            //}
    //            //else
    //            //{
    //            //    color = Colors.Blue; //only one correction
    //            //}

    //            ////if (word.CorrectionLength > 0 )
    //            //Colorize(word.CorrectionPosition, word.CorrectionLength, color);
    //        }
    //    }

    //    if (last < btext.GetModifiedText().Length)
    //        coloredText.Append(btext.GetModifiedText().Substring(last, btext.GetModifiedText().Length - last));

    //    return coloredText.ToString();
    //}
}