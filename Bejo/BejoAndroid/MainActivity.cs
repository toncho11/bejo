﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using System.IO;
using System.Json;
//using ServiceStack.Text;
//using System.Web.Script.Serialization;

namespace BejoAndroid
{
    public class BejoResult
    {
        public string CorrectedText { get; set; }
        public int[] numbers { get; set; }
    }


    [Activity(Label = "BejoAndroid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        EditText editText;

        TextView textView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button buttonProcess = FindViewById<Button>(Resource.Id.btnProcess);

            editText = FindViewById<EditText>(Resource.Id.inputText);

            textView = FindViewById<TextView>(Resource.Id.outputText);

            buttonProcess.Click += ButtonProcess_Click;

            editText.Text = GetData("GetDemoDocument", "");
        }

        private void ButtonProcess_Click(object sender, EventArgs e)
        {
            string result = GetData("ApplyCorrection", editText.Text);

            textView.Text = result;
        }

        public static string GetData(string command, string text)
        {
            JsonObject result = null;

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://bejo.micromedia.bg/ProcessHandler.ashx");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Proxy = null;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{  \"Command\": \"" + command+ "\", \"TextToCorrect\": \"" + text +"\" }";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string jsonResult = streamReader.ReadToEnd();
                    //button.Text = result;

                    JsonValue value = JsonValue.Parse(jsonResult);
                    result = value as JsonObject;
                    // result = JsonSerializer.DeserializeFromString<BejoResult>(jsonResult);
                    //result = new Serializer().Deserialize<BejoResult>(jsonResult);
                    //result = new JavaScriptSerializer().Deserialize<BejoResult>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                //if (result == null) result = new BejoResult();

                //result.CorrectedText = ex.Message;
            }

            return result["CorrectedText"]; ;
        }
    }
}

