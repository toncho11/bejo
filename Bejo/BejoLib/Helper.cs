﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BejoLib
{
    public class Helper
    {
        /// <summary>
        /// The folder which contains the "Dict" and "POS" folders for the dictionary and the tagger.
        /// </summary>
        /// <returns></returns>
        public static string GetBaseConfigFolder()
        {
            string path;

            #if (DEBUG)
            if (!IsCalledFromWebApp)
                path = System.IO.Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "..";   //"\\..\\..\\..\\..";
            else path = System.Web.HttpRuntime.AppDomainAppPath + ".." + Path.DirectorySeparatorChar + "..";   //"\\..\\..";
            #else
            if (!IsCalledFromWebApp)
                path = System.IO.Directory.GetCurrentDirectory();
            else path = System.Web.HttpRuntime.AppDomainAppPath.Remove(System.Web.HttpRuntime.AppDomainAppPath.Length-1);
            #endif

            return path;
        }

        public static string GetDictionaryFolder()
        {
            return Helper.GetBaseConfigFolder() + Path.DirectorySeparatorChar + "Dict";
        }

        public static string GetPOSFolder()
        {
           return Helper.GetBaseConfigFolder() + System.IO.Path.DirectorySeparatorChar + "POS";
        }

        public static string GetNLPModelsFolder()
        {
             return GetPOSFolder() + System.IO.Path.DirectorySeparatorChar + "models";
        }

        public static string GetDefaultDictionaryPath()
        {
            return GetDictionaryFolder() + Path.DirectorySeparatorChar + "lefff-3.4.mlex";
        }


        public static string BejoVersion
        {
            get
            {
                return "1.5";
            }
        }

        public static bool IsCalledFromWebApp
        {
            get
            {
                bool isCalledFromWebApp = System.Web.HttpRuntime.AppDomainAppId != null;

                return isCalledFromWebApp;
            }
        }
    }
}
