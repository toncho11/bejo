﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

namespace BejoLib
{
    /// <summary>
    /// Contans the original and modified text. Provides methods that modify the text and POS tagging without the need to redo the POS tagging after each change. Changes are done based on rules.
    /// </summary>
    public class BejoText
    {
        string TextOriginal;
        StringBuilder TextModified;

        List<List<BejoWord>> TaggedSentences;

        /// <summary>
        /// Allows the UI to underline and show the changes in the text to the user
        /// </summary>
        List<int> correctionsPositions; //start,length,start,length

        List<BejoWord> taggedWords;

        public BejoText(string textOriginal, List<List<BejoWord>> taggedSentences)
        {
            TextOriginal = textOriginal;
            TextModified = new StringBuilder(textOriginal); //must be string builder

            TaggedSentences = taggedSentences;

            taggedWords = new List<BejoWord>();
            correctionsPositions = new List<int>();

            //Apply corrections to tagging
            foreach (List<BejoWord> sentence in TaggedSentences)
                for (int i = 0; i < sentence.Count; i++)
                {
                    //temporary fix for N and NC
                    if (sentence[i].POSTaggedWord.tag() == "NC")
                        sentence[i].POSTaggedWord.setTag("N");

                    taggedWords.Add((BejoWord)sentence[i]);
                }
        }

        public BejoWord[] GetTaggedWords
        {
            get
            {
                return taggedWords.ToArray();
            }
        }

        public void ModifyTag(int index,string replacement)
        {
            string old = taggedWords[index].POSTaggedWord.value();

            //replace in text
            TextModified.Remove(taggedWords[index].POSTaggedWord.beginPosition(), old.Length);
            TextModified.Insert(taggedWords[index].POSTaggedWord.beginPosition(), replacement);

            //put positions for visualisation later
            correctionsPositions.Add(taggedWords[index].POSTaggedWord.beginPosition());
            correctionsPositions.Add(replacement.Length);

            int shifting = replacement.Length - taggedWords[index].POSTaggedWord.value().Length; //must be modified for each case

            //small correction
            if (char.IsUpper(old[0]))
            {
                TextModified[taggedWords[index].POSTaggedWord.beginPosition()] = char.ToUpper(TextModified[taggedWords[index].POSTaggedWord.beginPosition()]);
            }

            //modify tag
            taggedWords[index].POSTaggedWord.setValue(replacement); //missing if un is Un
            //BUG fix:
            //taggedWords[index].POSTaggedWord.setEndPosition(taggedWords[index].POSTaggedWord.endPosition() + shifting);
            taggedWords[index].POSTaggedWord.setEndPosition(taggedWords[index].POSTaggedWord.beginPosition() + replacement.Length);

            //adjust were previous corrections were already done
            for (int j = 0; j < correctionsPositions.Count; j += 2)
            {
                if (correctionsPositions[j] > taggedWords[index].POSTaggedWord.beginPosition())
                    correctionsPositions[j] += shifting;
            }

            //adjust all tags after the one being modified
            foreach (BejoWord w in taggedWords)
            {
                if (w.POSTaggedWord.beginPosition() > taggedWords[index].POSTaggedWord.beginPosition())
                {
                    w.POSTaggedWord.setBeginPosition(w.POSTaggedWord.beginPosition() + shifting);
                    w.POSTaggedWord.setEndPosition(w.POSTaggedWord.endPosition() + shifting);
                }
            }

            taggedWords[index].CurrentBestCorrection = replacement;

            //taggedWords[index].LastCorrectionRuleApplied = rule;
        }

        public string GetModifiedText()
        {
            return TextModified.ToString();
        }

        public int TaggedWordsCount
        {
            get
            {
                return (taggedWords == null) ? 0 : taggedWords.Count;
            }
        }

        public void Replace2TagsWithOne(int index1, int index2, string replacement)
        {
            char firstLetterOldExpression = taggedWords[index1].POSTaggedWord.value()[0];
            int lengthOldExpression = taggedWords[index2].POSTaggedWord.endPosition() - taggedWords[index1].POSTaggedWord.beginPosition();

            //replace in text
            TextModified.Remove(taggedWords[index1].POSTaggedWord.beginPosition(), lengthOldExpression);

            TextModified.Insert(taggedWords[index1].POSTaggedWord.beginPosition(), replacement);

            //put positions for visualisation later
            correctionsPositions.Add(taggedWords[index1].POSTaggedWord.beginPosition());
            correctionsPositions.Add(replacement.Length);

            int shifting = replacement.Length - lengthOldExpression; //must be modified for each case

            //small correction
            if (char.IsUpper(firstLetterOldExpression))
            {
                TextModified[taggedWords[index1].POSTaggedWord.beginPosition()] = char.ToUpper(TextModified[taggedWords[index1].POSTaggedWord.beginPosition()]);
            }

            //modify tag
            taggedWords[index1].POSTaggedWord.setValue(replacement); //missing if un is Un

            //BUG fix:
            //taggedWords[index1].POSTaggedWord.setEndPosition(taggedWords[index1].POSTaggedWord.endPosition() + shifting);
            taggedWords[index1].POSTaggedWord.setEndPosition(taggedWords[index1].POSTaggedWord.beginPosition() + replacement.Length);

            //delete second tag
            taggedWords.RemoveAt(index2);

            //adjust were previous corrections were already done
            for (int j = 0; j < correctionsPositions.Count; j += 2)
            {
                if (correctionsPositions[j] > taggedWords[index1].POSTaggedWord.beginPosition())
                    correctionsPositions[j] += shifting;
            }

            //adjust all tags after the one being modified
            foreach (BejoWord w in taggedWords)
            {
                if (w.POSTaggedWord.beginPosition() > taggedWords[index1].POSTaggedWord.beginPosition())
                {
                    w.POSTaggedWord.setBeginPosition(w.POSTaggedWord.beginPosition() + shifting);
                    w.POSTaggedWord.setEndPosition(w.POSTaggedWord.endPosition() + shifting);
                }
            }

            taggedWords[index1].CurrentBestCorrection = replacement;
        }

        public void DeleteFirstReplaceSecond(int index1, int index2, string replacement)
        {
            string old1 = taggedWords[index1].POSTaggedWord.value();
            //string old2 = taggedWords[index1].POSTaggedWord.value();

            //replace in text
            TextModified.Remove(taggedWords[index1].POSTaggedWord.beginPosition(), taggedWords[index2].POSTaggedWord.endPosition() - taggedWords[index1].POSTaggedWord.beginPosition());

            TextModified.Insert(taggedWords[index1].POSTaggedWord.beginPosition(), replacement);

            //put positions for visualisation later
            correctionsPositions.Add(taggedWords[index1].POSTaggedWord.beginPosition());
            correctionsPositions.Add(replacement.Length);

            int shifting = replacement.Length - (taggedWords[index2].POSTaggedWord.endPosition() - taggedWords[index1].POSTaggedWord.beginPosition()); //must be modified for each case

            //small correction
            if (char.IsUpper(old1[0]))
            {
                TextModified[taggedWords[index1].POSTaggedWord.beginPosition()] = char.ToUpper(TextModified[taggedWords[index1].POSTaggedWord.beginPosition()]);
            }

            //modify tag
            taggedWords[index2].POSTaggedWord.setValue(replacement); //missing if un is Un
            int start = taggedWords[index1].POSTaggedWord.beginPosition();
            int end = start + replacement.Length;
            taggedWords[index2].POSTaggedWord.setBeginPosition(start); //modify tag 2
            taggedWords[index2].POSTaggedWord.setEndPosition(end); //modify tag 2

            //delete second tag
            taggedWords.RemoveAt(index1);

            //adjust were previous corrections were already done
            for (int j = 0; j < correctionsPositions.Count; j += 2)
            {
                if (correctionsPositions[j] > taggedWords[index1].POSTaggedWord.beginPosition())
                    correctionsPositions[j] += shifting;
            }

            //adjust all tags after the one being modified
            foreach (BejoWord w in taggedWords)
            {
                if (w.POSTaggedWord.beginPosition() > taggedWords[index2 - 1].POSTaggedWord.beginPosition()) //index2-1 because of deletion in collection
                {
                    w.POSTaggedWord.setBeginPosition(w.POSTaggedWord.beginPosition() + shifting);
                    w.POSTaggedWord.setEndPosition(w.POSTaggedWord.endPosition() + shifting);
                }
            }

            taggedWords[index1].CurrentBestCorrection = replacement;
        }

        public void SetCorrectionWords(int i,BejoDictionaryWord[] words)
        {
            taggedWords[i].CorrectionWords = words;
        }

        public int[] GetCorrectionsPositionLength()
        {
            List<int> result = new List<int>();

            foreach (var word in taggedWords)
            {
                if (word.IsCorrected)
                {
                    if (!string.IsNullOrEmpty(word.CurrentBestCorrection))
                    {
                        result.Add(word.CorrectionPosition);
                        result.Add(word.CorrectionLength);
                    }
                }
            }

            return result.ToArray();
        }
    }
}
