﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

namespace BejoLib
{
    public class RuleCorrectEs : BejoCorrectionRule
    {

        public override string Description
        {
            get
            {
                return "Example: 'etudiant' is corrected to 'étudiant'";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversMostCasesButNotAll;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            int minWordLenghtToCheck = 3;

            for (int i = 0; i < btext.TaggedWordsCount; i++)
            {
                BejoWord word1 = btext.GetTaggedWords[i];

                if (BejoNLPhelper.ContainsAnyE(word1.POSTaggedWord.value()))
                {
                    //only nouns and adjectives are searched for now
                    BejoWordType typeWord1 = (word1.POSTaggedWord.tag() == "N") ? BejoWordType.Noun : (word1.POSTaggedWord.tag() == "ADJ") ? BejoWordType.Adjective : BejoWordType.Unknown;

                    string word1AllESame = BejoNLPhelper.ConvertToLowerAllE(word1.POSTaggedWord.value());

                    if (word1.POSTaggedWord.value().Length > minWordLenghtToCheck //for too small words we do not know what to do
                        && BejoNLPhelper.ContainsAnyE(word1.POSTaggedWord.value()))
                    {
                        
                        if (typeWord1 == BejoWordType.Noun || typeWord1 == BejoWordType.Adjective) //only these are in the db
                        {
                            //1. generate suggestions
                            BejoDictionaryWord[] suggestions = (from correctDictWord in BejoDictionary.BejoWordDictionary.AsParallel()
                                                                let lengthCorrectDictWord = correctDictWord.Content.Length
                                                                where
                                                                correctDictWord.ContainsAnyE
                                                                && (correctDictWord.Type == typeWord1) //not to change de,des
                                                                && word1AllESame == correctDictWord.AllESame //on "e" they match

                                                                //for adjectives if only the last 'e' is the difference than this is not enough to change it
                                                                && (!
                                                                     (
                                                                       correctDictWord.AllESame.EndsWith("e")
                                                                       && word1.POSTaggedWord.value().Substring(0, lengthCorrectDictWord - 1) == correctDictWord.Content.Substring(0, lengthCorrectDictWord - 1)
                                                                       && typeWord1 == BejoWordType.Adjective
                                                                     )
                                                                    )
                                                                select correctDictWord).ToArray();

                            //remove duplicates
                            suggestions = suggestions.GroupBy(x => x.Content).Select(x => x.First()).ToArray();

                            //2. choose which suggestions to use or not use at all
                            if (suggestions.Length > 0)
                            {
                                BejoDictionaryWord[] replacements = suggestions.Where(x => !x.Content.Equals(word1.POSTaggedWord.value(), StringComparison.OrdinalIgnoreCase)).ToArray(); //we remove the one used by the user
                                
                                string replacement = "";

                                if (replacements.Count() == 1) //we are sure then we change
                                {
                                    replacement = replacements[0].Content;//we choose the first
                                }
                                else
                                if (replacements.Count() > 1) //we do not change, but still the item will be colored
                                {
                                    replacement = word1.POSTaggedWord.value();//replacement is not empty
                                }

                                //apply the replacement
                                if (!string.IsNullOrEmpty(replacement))
                                {
                                    btext.ModifyTag(i, replacement);

                                    btext.SetCorrectionWords(i, suggestions);
                                }
                            }
                        }

                    }
                }
            }
            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectDetForNouns : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Corrects all un/une/des/le/la/les. Example: 'un pomme' => 'une pomme'";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversAllCasses;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            string[] detectionRule = { "DET", "N" };//N is custom! temporary fix

            TaggedWord tw = new TaggedWord();

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 1; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i]; //det
                BejoWord word2 = btext.GetTaggedWords[i + 1];

                //if detection rule matches
                if (word1.POSTaggedWord.tag() != detectionRule[0] || word2.POSTaggedWord.tag() != detectionRule[1])
                    continue;

                string det = word1.POSTaggedWord.value();
                string noun = word2.POSTaggedWord.value();

                CorrectDet(ref btext, det, noun, i);
            }

            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectAdjectives : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Example: 'bonne système ' => 'bon système'";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversAllCasses;
            }
        }

        public override string Execute(ref BejoText btext)
        {

            string[] detectionRule = { "N", "ADJ" };//N is custom! temporary fix

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 1; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i];
                BejoWord word2 = btext.GetTaggedWords[i + 1];

                string noun = "";
                string adjective = "";

                //systeme joli
                bool isAdjectiveAfterNoun = false;
                if (word1.POSTaggedWord.tag() == detectionRule[0] && word2.POSTaggedWord.tag() == detectionRule[1])
                {
                    isAdjectiveAfterNoun = true;
                    noun = word1.POSTaggedWord.value();
                    adjective = word2.POSTaggedWord.value();
                }

                //bon appétit
                if (word1.POSTaggedWord.tag() == detectionRule[1] && word2.POSTaggedWord.tag() == detectionRule[0])
                {
                    isAdjectiveAfterNoun = false;
                    noun = word2.POSTaggedWord.value();
                    adjective = word1.POSTaggedWord.value();
                }

                if (!noun.Equals("") && !adjective.Equals(""))
                    CorrectAdjective(ref btext, noun, adjective, isAdjectiveAfterNoun, i);
            }

            return btext.GetModifiedText();
        }

    }

    public class RuleCorrectDetForNounsWithAdjectiveBetween : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Corrects all un/une/des/le/la/les. Example: 'un bonne système' => 'un bon système'. This rule handles the case when there is an adjective between the det and the noun.";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversAllCasses;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            string[] detectionRule = { "DET", "ADJ", "N" };//N is custom! temporary fix

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 2; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i];
                BejoWord word2 = btext.GetTaggedWords[i + 1];
                BejoWord word3 = btext.GetTaggedWords[i + 2];

                //if detection rule matches
                if (word1.POSTaggedWord.tag() != detectionRule[0] || word2.POSTaggedWord.tag() != detectionRule[1] || word3.POSTaggedWord.tag() != detectionRule[2])
                    continue;

                string det = word1.POSTaggedWord.value();
                string noun = word3.POSTaggedWord.value();

                CorrectDet(ref btext, det, noun, i);
            }

            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectVerbDerivedAdj1 : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Fixes verb derived adjectives. Example: Je mange une pomme sucré. => Je mange une pomme sucrée.";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversOneThird;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            string[] detectionRule = { "N", "VPP" };//N is custom! temporary fix

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 1; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i];
                BejoWord word2 = btext.GetTaggedWords[i + 1];

                string noun = "";
                string adjective = "";

                //systeme joli
                bool isAdjectiveAfterNoun = false;
                if (word1.POSTaggedWord.tag() == detectionRule[0] && word2.POSTaggedWord.tag() == detectionRule[1])
                {
                    isAdjectiveAfterNoun = true;
                    noun = word1.POSTaggedWord.value();
                    adjective = word2.POSTaggedWord.value();
                }

                //bon appétit
                if (word1.POSTaggedWord.tag() == detectionRule[1] && word2.POSTaggedWord.tag() == detectionRule[0])
                {
                    isAdjectiveAfterNoun = false;
                    noun = word2.POSTaggedWord.value();
                    adjective = word1.POSTaggedWord.value();
                }

                CorrectAdjective(ref btext, noun, adjective, isAdjectiveAfterNoun, i);
            }

            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectVerbDerivedAdj2 : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Fixes verb derived adjectives. Example: La femme est épuisé. => La femme est épuisée.";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversOneThird;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            string[] detectionRule = { "N", "V", "VPP" };//N is custom! temporary fix

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 2; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i];
                BejoWord word2 = btext.GetTaggedWords[i + 1];
                BejoWord word3 = btext.GetTaggedWords[i + 2];

                string noun = "";
                string verb = "";
                string adjective = ""; //VPP

                if (word1.POSTaggedWord.tag() == detectionRule[0] && word2.POSTaggedWord.tag() == detectionRule[1] && word3.POSTaggedWord.tag() == detectionRule[2])
                {
                    noun = word1.POSTaggedWord.value();
                    verb = word2.POSTaggedWord.value();
                    adjective = word3.POSTaggedWord.value();
                }

                bool isVerbEtre = BejoNLPhelper.IsVerbEtre(verb);

                if (!isVerbEtre)
                    continue;

                BejoDictionaryWord[] nounCandidates = BejoDictionary.BejoWordDictionary.AsParallel().Where(m => m.Type == BejoWordType.Noun && m.Content.ToLower() == noun.ToLower()).ToArray();

                //get adjective used
                BejoDictionaryWord[] adjectiveUsed = BejoDictionary.BejoWordDictionary.AsParallel().Where(m => m.Type == BejoWordType.Adjective && m.Content.ToLower() == adjective.ToLower()).ToArray();

                if (adjectiveUsed.Length == 0)
                    continue;

                //search for adjective that is correct for this noun with mainform of the adjective used 
                BejoDictionaryWord[] suggestedAdjectives = BejoDictionary.BejoWordDictionary.AsParallel().Where(n =>

                    n.MainForm.Equals(adjectiveUsed[0].MainForm, StringComparison.OrdinalIgnoreCase)

                    && n.Type == BejoWordType.Adjective

                    && (n.Number == nounCandidates[0].Number || n.Number == BejoNumber.Invariant)
                    && (n.Gender == nounCandidates[0].Gender || n.Gender == BejoGender.Invariant)

                    ).ToArray();

                if (suggestedAdjectives.Length > 0 && !suggestedAdjectives.Contains(adjectiveUsed[0]))
                {
                    string replacement = suggestedAdjectives[0].Content;
                    btext.ModifyTag(i + 2, replacement);
                }
            }

            return btext.GetModifiedText();
        }
    }

    /// <summary>
    /// Depends on RuleCorrectDetAbbreviation to be executed first
    /// </summary>
    public class MergingPreDet : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Does the merging of the type: de + le = du";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversAllCasses;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            //correct: Le livre du garçon
            //correct: Il m'a dit de le faire
            string[] detectionRule = { "P", "DET" };//N is custom! temporary fix

            TaggedWord tw = new TaggedWord();

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 1; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i];
                BejoWord word2 = btext.GetTaggedWords[i + 1];

                //if detection rule matches
                if (word1.POSTaggedWord.tag() != detectionRule[0] || word2.POSTaggedWord.tag() != detectionRule[1])
                    continue;

                string P = word1.POSTaggedWord.value();
                string det2 = word2.POSTaggedWord.value();

                string replacement;

                // de 
                if (P.Equals("de", StringComparison.OrdinalIgnoreCase) && det2.Equals("le", StringComparison.OrdinalIgnoreCase))
                {
                    //1. correction rule
                    replacement = "du";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("de", StringComparison.OrdinalIgnoreCase) && det2.Equals("les", StringComparison.OrdinalIgnoreCase))
                {
                    //2. correction rule
                    replacement = "des";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("de", StringComparison.OrdinalIgnoreCase) && det2.Equals("lequel", StringComparison.OrdinalIgnoreCase))
                {
                    //3. correction rule
                    replacement = "duquel";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("de", StringComparison.OrdinalIgnoreCase) && det2.Equals("lesquels", StringComparison.OrdinalIgnoreCase))
                {
                    //4. correction rule
                    replacement = "desquels";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("de", StringComparison.OrdinalIgnoreCase) && det2.Equals("lesquelles", StringComparison.OrdinalIgnoreCase))
                {
                    //5. correction rule
                    replacement = "desquelles";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }

                // à
                else
                if (P.Equals("à", StringComparison.OrdinalIgnoreCase) && det2.Equals("le", StringComparison.OrdinalIgnoreCase))
                {
                    //6. correction rule
                    replacement = "au";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("à", StringComparison.OrdinalIgnoreCase) && det2.Equals("les", StringComparison.OrdinalIgnoreCase))
                {
                    //7. correction rule
                    replacement = "aux";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("à", StringComparison.OrdinalIgnoreCase) && det2.Equals("lequel", StringComparison.OrdinalIgnoreCase))
                {
                    //8. correction rule
                    replacement = "auquel";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("à", StringComparison.OrdinalIgnoreCase) && det2.Equals("lesquels", StringComparison.OrdinalIgnoreCase))
                {
                    //9. correction rule
                    replacement = "auxquels";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
                else
                if (P.Equals("à", StringComparison.OrdinalIgnoreCase) && det2.Equals("lesquelles", StringComparison.OrdinalIgnoreCase))
                {
                    //10. correction rule
                    replacement = "auxquelles";

                    btext.Replace2TagsWithOne(i, i + 1, replacement);
                }
            }

            return btext.GetModifiedText();
        }
    }

    /// <summary>
    /// Source http://www.francaisfacile.com/exercices/exercice-francais-2/exercice-francais-60938.php
    /// Source 2: http://grammaire.reverso.net/5_6_02_elision.shtml
    /// </summary>
    public class RuleCorrectAbbreviations : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "L’élision. Example: le occasion => l'occasion; Je ai => J'ai; que au => qu'au";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversMostCasesButNotAll;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            TaggedWord tw = new TaggedWord();

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 1; i++)
            {
                //Detection
                BejoWord tagWord1 = btext.GetTaggedWords[i]; //det
                BejoWord tagWord2 = btext.GetTaggedWords[i + 1];

                string word1 = tagWord1.POSTaggedWord.value();
                string word2 = tagWord2.POSTaggedWord.value();

                bool startsWithVowelsOrHmuted = BejoNLPhelper.StartsWithVowelOrMuteH(word2);

                bool isDet = word1.Equals("le", StringComparison.OrdinalIgnoreCase) || word1.Equals("la", StringComparison.OrdinalIgnoreCase);

                if ((isDet || word1.ToLower().EndsWith("e")) && startsWithVowelsOrHmuted) //should we process
                {
                    if ((word1.ToLower() != "presque" && word1.ToLower() != "quelque" && word1.ToLower() != "quelconque") //avoid some cases
                        &&
                       (//every line is a positive match that forces a correction:

                        (isDet && tagWord2.POSTaggedWord.tag() == "ADJ") || //l'ancienne
                        (isDet && tagWord2.POSTaggedWord.tag() == "N") || // l'acteur
                        (isDet && tagWord2.POSTaggedWord.tag() == "V") || // Je l'aime.

                        //J'ai tout ce qu'il me faut.
                        (tagWord1.POSTaggedWord.tag() == "PROREL" && word2.ToLower() == "il") ||

                        //J'ai, but not: elle a => ell'a                                                 
                        (tagWord1.POSTaggedWord.tag() == "CLS" && word1.ToLower() != "elle" && tagWord2.POSTaggedWord.tag() == "V") ||

                        //qu'elle
                        (tagWord1.POSTaggedWord.tag() == "CS" && tagWord2.POSTaggedWord.tag() == "CLS") ||

                        //Cela vient d'eux. de anciennes -> d'anciennes
                        (tagWord1.POSTaggedWord.tag() == "P" && word1.ToLower() != "entre") ||

                        // qu'au
                        (tagWord1.POSTaggedWord.tag() == "CS" && tagWord2.POSTaggedWord.tag() == "P") ||

                        // jusqu'au 
                        (tagWord1.POSTaggedWord.tag() == "ADV" && tagWord2.POSTaggedWord.tag() == "P") ||

                        // lorsqu'enfine - jusque, lorsque, puisque et quoique marquent l'élision par l'apostrophe.
                        (tagWord1.POSTaggedWord.tag() == "CS" && tagWord2.POSTaggedWord.tag() == "ADV") ||

                        //parce qu'au - Les locutions composées avec que (parce que, bien que, avant que…) se comportent comme si que était employé seul.
                        (tagWord1.POSTaggedWord.tag() == "C" && tagWord2.POSTaggedWord.tag() == "P") ||

                        //C'est
                        (word1.ToLower() == "ce" && word2.ToLower() == "est") ||

                        //Pour que on vous suive, montrez votre détermination => qu'on
                        (tagWord1.POSTaggedWord.tag() == "C" && word2.ToLower() == "on")

                       )
                      )
                    {
                        string replacement = word1.Substring(0, word1.Length - 1) + "'" + word2;
                        btext.DeleteFirstReplaceSecond(i, i + 1, replacement);
                    }

                }

                // si ; with "qui" there is no élision
                //if ((word1.ToLower().Equals("si")) && startsWithVowelsOrHmuted)
                //{
                //    if (tagWord2.POSTaggedWord.tag() != "ADV" && tagWord2.POSTaggedWord.tag() != "ADJ")
                //    {
                //        string replacement = word1.Substring(0, word1.Length - 1) + "'" + word2;
                //        btext.DeleteFirstReplaceSecond(i, i + 1, replacement);
                //    }
                //}

                //Les éducateurs restent au foyer s’il y a des enfants malades.
                if (word1.ToLower().Equals("si", StringComparison.InvariantCultureIgnoreCase) 
                    && (word2.Equals("il", StringComparison.InvariantCultureIgnoreCase)
                        || word2.Equals("ils", StringComparison.InvariantCultureIgnoreCase)
                        )
                   )
                {
                    string replacement = word1.Substring(0, word1.Length - 1) + "'" + word2;
                    btext.DeleteFirstReplaceSecond(i, i + 1, replacement);
                } 
            }

            return btext.GetModifiedText();
        }
    }
    public class RuleCorrectOdansE : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Corrects Œ: coeur => cœur ";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversMostCasesButNotAll;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            for (int i = 0; i < btext.TaggedWordsCount; i++)
            {

                BejoWord tagWord = btext.GetTaggedWords[i];
                if (tagWord.POSTaggedWord.value().ToLower().Equals("coeur"))
                    btext.ModifyTag(i, "cœur");
                else
                if (tagWord.POSTaggedWord.value().ToLower().Equals("soeur"))
                    btext.ModifyTag(i, "sœur");
                else
                if (tagWord.POSTaggedWord.value().ToLower().Equals("oeil"))
                    btext.ModifyTag(i, "œil");
                else
                if (tagWord.POSTaggedWord.value().ToLower().Equals("oeuf"))
                    btext.ModifyTag(i, "œuf");
                else
                if (tagWord.POSTaggedWord.value().ToLower().Equals("foetus"))
                    btext.ModifyTag(i, "fœtus");
                else
                if (tagWord.POSTaggedWord.value().ToLower().Equals("oestrogène"))
                    btext.ModifyTag(i, "œstrogène");
                else
                if (tagWord.POSTaggedWord.value().ToLower().Equals("moeurs"))
                    btext.ModifyTag(i, "mœurs");
            }

            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectAdjPosessif : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Corrects all mon/ma. Example: 'ma job' => 'mon job', 'ta problème' = > 'ton problème'";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversMostCasesButNotAll;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            TaggedWord tw = new TaggedWord();

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 1; i++)
            { 
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i]; //det
                BejoWord word2 = btext.GetTaggedWords[i + 1];

                string det = word1.POSTaggedWord.value();
                string noun = word2.POSTaggedWord.value();


                if (det.ToLower() != "ma" && det.ToLower() != "mon" && det.ToLower() != "ton" && det.ToLower() != "ta" && det.ToLower() != "son" && det.ToLower() != "sa")
                    continue;

                if (word2.POSTaggedWord.tag() != "N" && word2.POSTaggedWord.tag() != "ET") //C'est ta amie. gives "/ET" for a tag.
                    continue;

                //if unknown word then skip
                if (!(BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Type == BejoWordType.Noun && m.Content.ToLower() == noun.ToLower()) > 0))
                continue;

                string oldDet = "";
                string replacement = "";

                //1.correction rule
                oldDet = "mon";
                replacement = "ma";
                if (det.ToLower() == oldDet
                    && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number == BejoNumber.Singular) > 0
                    && !BejoNLPhelper.StartsWithVowelOrMuteH(noun)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //2. correction rule
                oldDet = "ma";
                replacement = "mon";
                if (det.ToLower() == oldDet
                    && (BejoNLPhelper.StartsWithVowelOrMuteH(noun)
                         || BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number == BejoNumber.Singular) > 0)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //3.correction rule
                oldDet = "ton";
                replacement = "ta";
                if (det.ToLower() == oldDet
                    && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number == BejoNumber.Singular) > 0
                    && !BejoNLPhelper.StartsWithVowelOrMuteH(noun)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //4. correction rule
                oldDet = "ta";
                replacement = "ton";
                if (det.ToLower() == oldDet
                    && (BejoNLPhelper.StartsWithVowelOrMuteH(noun)
                         || BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number == BejoNumber.Singular) > 0)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //5.correction rule
                oldDet = "son";
                replacement = "sa";
                if (det.ToLower() == oldDet
                    && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number == BejoNumber.Singular) > 0
                    && !BejoNLPhelper.StartsWithVowelOrMuteH(noun)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //6. correction rule
                oldDet = "sa";
                replacement = "son";
                if (det.ToLower() == oldDet
                    && (BejoNLPhelper.StartsWithVowelOrMuteH(noun)
                         || BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number == BejoNumber.Singular) > 0)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //7. 
                replacement = "mes";
                if ((det.ToLower() == "mon" || det.ToLower() == "ma") && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //8. 
                replacement = "ses";
                if ((det.ToLower() == "son" || det.ToLower() == "sa") && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //9. 
                replacement = "tes";
                if ((det.ToLower() == "ton" || det.ToLower() == "ta") && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }
            }

            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectAdjPosessifAdjectiveBetween : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Corrects all mon/ma. Example: 'mon jolie école' => 'ma jolie école'";
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversMostCasesButNotAll;
            }
        }

        public override string Execute(ref BejoText btext)
        {
            TaggedWord tw = new TaggedWord();

            //Process detection and apply corrections
            for (int i = 0; i < btext.TaggedWordsCount - 2; i++)
            {
                //Detection
                BejoWord word1 = btext.GetTaggedWords[i];
                BejoWord word2 = btext.GetTaggedWords[i + 1];
                BejoWord word3 = btext.GetTaggedWords[i + 2];

                string det = word1.POSTaggedWord.value();
                string adj = word2.POSTaggedWord.value();
                string noun = word3.POSTaggedWord.value();


                if (det.ToLower() != "ma" && det.ToLower() != "mon" && det.ToLower() != "ton" && det.ToLower() != "ta" && det.ToLower() != "son" && det.ToLower() != "sa")
                    continue;

                if (word3.POSTaggedWord.tag() != "N" && word3.POSTaggedWord.tag() != "ET") //C'est ta amie. gives "/ET" for a tag.
                    continue;

                //if unknown word then skip
                if (!(BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Type == BejoWordType.Noun && m.Content.ToLower() == noun.ToLower()) > 0))
                    continue;

                string oldDet = "";
                string replacement = "";

                //1.correction rule
                oldDet = "mon";
                replacement = "ma";
                if (det.ToLower() == oldDet
                    && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number == BejoNumber.Singular) > 0
                    && !BejoNLPhelper.StartsWithVowelOrMuteH(adj)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //2. correction rule
                oldDet = "ma";
                replacement = "mon";
                if (det.ToLower() == oldDet
                    && (BejoNLPhelper.StartsWithVowelOrMuteH(adj)
                         || BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number == BejoNumber.Singular) > 0)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //3.correction rule
                oldDet = "ton";
                replacement = "ta";
                if (det.ToLower() == oldDet
                    && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number == BejoNumber.Singular) > 0
                    && !BejoNLPhelper.StartsWithVowelOrMuteH(adj)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //4. correction rule
                oldDet = "ta";
                replacement = "ton";
                if (det.ToLower() == oldDet
                    && (BejoNLPhelper.StartsWithVowelOrMuteH(adj)
                         || BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number == BejoNumber.Singular) > 0)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //5.correction rule
                oldDet = "son";
                replacement = "sa";
                if (det.ToLower() == oldDet
                    && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number == BejoNumber.Singular) > 0
                    && !BejoNLPhelper.StartsWithVowelOrMuteH(adj)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //6. correction rule
                oldDet = "sa";
                replacement = "son";
                if (det.ToLower() == oldDet
                    && (BejoNLPhelper.StartsWithVowelOrMuteH(adj)
                         || BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number == BejoNumber.Singular) > 0)
                    )
                {
                    btext.ModifyTag(i, replacement);
                }

                //7. 
                replacement = "mes";
                if ((det.ToLower() == "mon" || det.ToLower() == "ma") && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //8. 
                replacement = "ses";
                if ((det.ToLower() == "son" || det.ToLower() == "sa") && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //9. 
                replacement = "tes";
                if ((det.ToLower() == "ton" || det.ToLower() == "ta") && BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Content.ToLower() == noun.ToLower() && m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }
            }

            return btext.GetModifiedText();
        }
    }

    public class RuleCorrectSimpleConjugation : BejoCorrectionRule
    {
        public override string Description
        {
            get
            {
                return "Example: 'Je te dit.' => 'Je te dis.'. 3 tesnes are supported: present, passé composé, futur proche."; //futur simple, l'imparfait
            }
        }

        public override BejoImplementationRuleStatus Staus
        {
            get
            {
                return BejoImplementationRuleStatus.CoversOneThird;//not true - covers basic cases
            }
        }

        public override string Execute(ref BejoText btext)
        {
            //External
            //1. Expand Je t'aime => Je te aime. Otherwise tagging does not work well. 

            //External
            //2. Generate BejoTriple(subject, auxiliary verb, verb) of type (BejoWord,BejoWord,BejoWord)

            //3. Apply some rules to identify wrong conjugation
            //Analyze all the BejoTriples and detect anomalies.

            //4. Use the information from BejoTriple to locate the verb in the text and replace it with the form selected in step 3

            return btext.GetModifiedText();
        }
    }

}
