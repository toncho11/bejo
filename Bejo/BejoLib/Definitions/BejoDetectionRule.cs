﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

namespace BejoLib
{
    interface BejoDetectionRule
    {
        string[] Detect(List<List<TaggedWord>> posTaggedSentences);
    }
}
