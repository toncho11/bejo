﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BejoLib
{
    public enum BejoImplementationRuleStatus
    {
        Initial,
        CoversOneThird,
        CoversMostCasesButNotAll,
        CoversAllCasses
    }
}
