﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BejoLib
{
    public abstract class BejoCorrectionRule
    {
        public abstract string Execute(ref BejoText btext);

        public abstract string Description { get;}

        public abstract BejoImplementationRuleStatus Staus { get; }

        protected void CorrectAdjective(ref BejoText btext, string noun, string adjective, bool isAdjectiveAfterNoun, int i)
        {
            string adjectiveLower = adjective.ToLower();

            //if unknown word then skip
            if (!BejoDictionary.BejoWordDictionary.Exists(m => m.Type == BejoWordType.Noun && m.Content == noun))
                return;

            BejoDictionaryWord[] nounCandidates = BejoDictionary.BejoWordDictionary.AsParallel().Where(m => m.Type == BejoWordType.Noun && m.Content.ToLower() == noun.ToLower()).ToArray();

            bool IsSpecialAdjective = (
                adjectiveLower.Equals("ce") || adjectiveLower.Equals("cette") || adjectiveLower.Equals("ces") || adjectiveLower.Equals("cet") 
                || adjectiveLower.Equals("bel") || adjectiveLower.Equals("beau") || adjectiveLower.Equals("belle") || adjectiveLower.Equals("beaux") || adjectiveLower.Equals("belles")
                || adjectiveLower.Equals("nouvel") || adjectiveLower.Equals("nouveau") || adjectiveLower.Equals("nouvelle") || adjectiveLower.Equals("nouveaux") || adjectiveLower.Equals("nouvelles")
                || adjectiveLower.Equals("vieux") || adjectiveLower.Equals("vieille") || adjectiveLower.Equals("vieil") || adjectiveLower.Equals("vieilles"));

            string replacement = "";

            if (IsSpecialAdjective)
            {
                replacement = ProcessSpecialAdjectives(adjectiveLower, nounCandidates[0]);
            }

            if (string.IsNullOrEmpty(replacement))
            {
                //get adjective used
                BejoDictionaryWord[] adjectiveUsed = BejoDictionary.BejoWordDictionary.AsParallel().Where(m => m.Type == BejoWordType.Adjective && m.Content.ToLower() == adjectiveLower).ToArray();

                if (adjectiveUsed.Length == 0)
                    return;

                string adjMainFormLowerAllESame = BejoNLPhelper.ConvertToLowerAllE(adjectiveUsed[0].MainForm);
                //search for an adjective that is correct for this noun with main form of the adjective used 
                BejoDictionaryWord[] suggestedAdjectives = BejoDictionary.BejoWordDictionary.AsParallel().Where(n =>

                    n.MainForm.Equals(adjectiveUsed[0].MainForm, StringComparison.OrdinalIgnoreCase)
                    //BejoNLPhelper.ConvertToLowerAllE(n.MainForm).Equals(adjMainFormLowerAllESame)

                    && n.Type == BejoWordType.Adjective

                    //Bug fix: added nounCandidates[0].Number == BejoNumber.Invariant
                    && (n.Number == nounCandidates[0].Number || n.Number == BejoNumber.Invariant || nounCandidates[0].Number == BejoNumber.Invariant)
                    && (n.Gender == nounCandidates[0].Gender || n.Gender == BejoGender.Invariant || nounCandidates[0].Gender == BejoGender.Invariant)

                    ).ToArray();

                if (suggestedAdjectives.Length > 0 && !suggestedAdjectives.Contains(adjectiveUsed[0]))
                {
                    replacement = suggestedAdjectives[0].Content;
                }
            }
           
            //actual correction
            if (!string.IsNullOrEmpty(replacement) && !replacement.Equals(adjective))
                btext.ModifyTag(((isAdjectiveAfterNoun) ? i += 1 : i), replacement);

        }

        private string ProcessSpecialAdjectives(string adjective, BejoDictionaryWord noun)
        {
            string replacement = "";

            //we check the noun or the adjective that follows
            bool startsWithVowelOrMuteH = BejoNLPhelper.StartsWithVowelOrMuteH(noun.Content);// : startsWithVowelOrMuteH = BejoNLPhelper.StartsWithVowelOrMuteH(adjective);

            //ce ==================================================================
            if (
                (adjective.Equals("ce") || adjective.Equals("cette") || adjective.Equals("ces"))
                && noun.Gender == BejoGender.Male
                && noun.Number == BejoNumber.Singular
                && startsWithVowelOrMuteH)
                replacement = "cet";
            else
            if (
                (adjective.Equals("cet") || adjective.Equals("cette") || adjective.Equals("ces"))
                && noun.Gender == BejoGender.Male
                && noun.Number == BejoNumber.Singular
                && !startsWithVowelOrMuteH)
                replacement = "ce";
            else
            if (
                (adjective.Equals("ce") || adjective.Equals("cet") || adjective.Equals("ces"))
                && noun.Gender == BejoGender.Female
                && noun.Number == BejoNumber.Singular
                && !startsWithVowelOrMuteH)
                replacement = "cette";
            else
            if (
                (adjective.Equals("ce") || adjective.Equals("cet") || adjective.Equals("cette"))
                && noun.Number == BejoNumber.Plural
                && !startsWithVowelOrMuteH)
                replacement = "ces";
            else
            //beau ==================================================================
            if (
                (adjective.Equals("beau") || adjective.Equals("belle") || adjective.Equals("beaux") || adjective.Equals("belles"))
                && noun.Gender == BejoGender.Male
                && noun.Number == BejoNumber.Singular
                && startsWithVowelOrMuteH)
                replacement = "bel";

            if (//set regular masculine singular
              (adjective.Equals("bel") || adjective.Equals("belle") || adjective.Equals("beaux") || adjective.Equals("belles"))
              && noun.Gender == BejoGender.Male
              && noun.Number == BejoNumber.Singular
              && !startsWithVowelOrMuteH)
                replacement = "beau";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("bel") || adjective.Equals("belle") || adjective.Equals("beau") || adjective.Equals("belles"))
              && noun.Gender == BejoGender.Male
              && noun.Number == BejoNumber.Plural)
                replacement = "beaux";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("bel") || adjective.Equals("beau") || adjective.Equals("beaux") || adjective.Equals("belles"))
              && noun.Gender == BejoGender.Female
              && noun.Number == BejoNumber.Singular)
                replacement = "belle";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("bel") || adjective.Equals("beau") || adjective.Equals("beaux") || adjective.Equals("belle"))
              && noun.Gender == BejoGender.Female
              && noun.Number == BejoNumber.Plural)
                replacement = "belles";
            else
            //nouveau ==================================================================
            if (
                (adjective.Equals("nouveau") || adjective.Equals("nouvelle") || adjective.Equals("nouveaux") || adjective.Equals("nouvelles"))
                && noun.Gender == BejoGender.Male
                && noun.Number == BejoNumber.Singular
                && startsWithVowelOrMuteH)
                replacement = "nouvel";

            if (//set regular masculine singular
              (adjective.Equals("nouvel") || adjective.Equals("nouvelle") || adjective.Equals("nouveaux") || adjective.Equals("nouvelles"))
              && noun.Gender == BejoGender.Male
              && noun.Number == BejoNumber.Singular
              && !startsWithVowelOrMuteH)
                replacement = "nouveau";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("nouvel") || adjective.Equals("nouvelle") || adjective.Equals("nouveau") || adjective.Equals("nouvelles"))
              && noun.Gender == BejoGender.Male
              && noun.Number == BejoNumber.Plural)
                replacement = "nouveaux";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("nouvel") || adjective.Equals("nouveau") || adjective.Equals("nouveaux") || adjective.Equals("nouvelles"))
              && noun.Gender == BejoGender.Female
              && noun.Number == BejoNumber.Singular)
                replacement = "nouvelle";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("nouvel") || adjective.Equals("nouveau") || adjective.Equals("nouveaux") || adjective.Equals("nouvelle"))
              && noun.Gender == BejoGender.Female
              && noun.Number == BejoNumber.Plural)
                replacement = "nouvelles";
            else
            //vieux ==================================================================
            if (
                (adjective.Equals("vieux") || adjective.Equals("vieille") || adjective.Equals("vieilles"))
                && noun.Gender == BejoGender.Male
                && noun.Number == BejoNumber.Singular
                && startsWithVowelOrMuteH)
                replacement = "vieil";

            if (//set regular masculine singular
              (adjective.Equals("vieil") || adjective.Equals("vieille") || adjective.Equals("vieilles"))
              && noun.Gender == BejoGender.Male
              && noun.Number == BejoNumber.Singular
              && !startsWithVowelOrMuteH)
                replacement = "vieux";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("vieil") || adjective.Equals("vieille") || adjective.Equals("vieilles"))
              && noun.Gender == BejoGender.Male
              && noun.Number == BejoNumber.Plural)
                replacement = "vieux";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("vieil") || adjective.Equals("vieilles") || adjective.Equals("vieil"))
              && noun.Gender == BejoGender.Female
              && noun.Number == BejoNumber.Singular)
                replacement = "vieille";
            else
            if (//set the plural for masculine pluriel
              (adjective.Equals("vieux") || adjective.Equals("vieil") || adjective.Equals("vieille"))
              && noun.Gender == BejoGender.Female
              && noun.Number == BejoNumber.Plural)
                replacement = "vieilles";

            return replacement;
        }

        protected void CorrectDet(ref BejoText btext, string det, string noun, int i)
        {
            //if unknown word then skip
            if (!(BejoDictionary.BejoWordDictionary.AsParallel().Count(m => m.Type == BejoWordType.Noun && m.Content.ToLower() == noun.ToLower()) > 0))
                return;

            bool detDefinitve = (new string[] { "le", "la", "les" }).Contains(det.ToLower());
            bool detNonDefinitive = (new string[] { "un", "une", "des" }).Contains(det.ToLower());

            string oldDet = "";
            string replacement = "";

            string nounLower = noun.ToLower();
            BejoDictionaryWord[] matchesByContent = BejoDictionary.BejoWordDictionary.AsParallel().Where(m => m.Content.ToLower() == nounLower).ToArray();

            if (detNonDefinitive)
            {
                //1. correction rule
                oldDet = "un";
                replacement = "une";
                if (det.ToLower() == oldDet && matchesByContent.Count(m => m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number != BejoNumber.Plural) > 0) //single, invariant are OK
                {
                    btext.ModifyTag(i, replacement);
                }

                //2. correction rule
                oldDet = "une";
                replacement = "un";
                if (det.ToLower() == oldDet && matchesByContent.Count(m => m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number != BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //6. plural noun with wrong det un/une
                replacement = "des";
                if ((det.ToLower() == "un" || det.ToLower() == "une") && matchesByContent.Count(m => m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }
            }

            if (detDefinitve)
            {
                //3. correction rule
                oldDet = "le";
                replacement = "la";
                if (det.ToLower() == oldDet && matchesByContent.Count(m => m.Type == BejoWordType.Noun && m.Gender == BejoGender.Female && m.Number != BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //4. correction rule
                oldDet = "la";
                replacement = "le";
                if (det.ToLower() == oldDet && matchesByContent.Count(m => m.Type == BejoWordType.Noun && m.Gender == BejoGender.Male && m.Number != BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }

                //5. plural noun with wrong det le/la
                replacement = "les";
                if ((det.ToLower() == "le" || det.ToLower() == "la") && matchesByContent.Count(m => m.Type == BejoWordType.Noun && m.Number == BejoNumber.Plural) > 0)
                {
                    btext.ModifyTag(i, replacement);
                }
            }
        }
    }
}
