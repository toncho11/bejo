﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BejoLib
{
    public class BejoTest
    {
        BejoDocument document;

        public BejoTest(BejoDocument doc)
        {
            document = doc;
            BejoCorrectedText = "";
            IsOK = false;
        }

        public string BejoCorrectedText { get; set; }

        public bool IsOK { get; set; }

        public string WrongText
        {
            set { document.WrongText = value; }
            get { return document.WrongText; }
        }

        public string TrueText
        {
            set { document.TrueText = value; }
            get { return document.TrueText; }
        }

        public bool IsExtendedMode
        {
            set { document.IsExtendedMode = value; }
            get { return document.IsExtendedMode; }
        }

        public int DocumentID
        {
            set { document.ID = value; }
            get { return document.ID; }
        }

        public string POStext
        {
            get; set;
        }
    }
}
