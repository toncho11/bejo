﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BejoLib
{
    [Serializable]
    public struct BejoDictionaryWord
    {
        /// <summary>
        /// Word
        /// </summary>
        public string Content;

        public BejoWordType Type;

        public BejoGender Gender;

        //public bool IsMaleFemaleFormSame;

        public string MainForm;

        public BejoNumber Number;

        /// <summary>
        /// Participe Passé à valeur d'adjectif qualificatif
        /// </summary>
        public bool IsParticipePasseAdjectif;

        public string ToExtendedString
        {
            get
            {
                return Content +
                    " " + ((Type == BejoWordType.Noun) ? "noun" : (Type == BejoWordType.Adjective) ? "adj." : "") +
                    " " + ((Gender == BejoGender.Male) ? "m." : (Gender == BejoGender.Female) ? "f." : "inv.") +
                    " " + ((Number == BejoNumber.Singular) ? "sg." : (Number == BejoNumber.Plural) ? "pl." : "inv.");
            }
        }

        private string allESame;
        public string AllESame
        {
            get
            {
                if (string.IsNullOrEmpty(allESame))
                {
                    allESame = BejoNLPhelper.ConvertToLowerAllE(Content);
                }

                return allESame;
            }
        }

        /// <summary>
        /// Used with AllESame
        /// </summary>
        public bool ContainsAnyE;
    }
}
