﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BejoLib
{
    public struct BejoTriple
    {
        public BejoWord Subject;
        public BejoWord AuxiliaryVerb; //etre, avoir , aller
        public BejoWord MainVerb; //present tense has only this, no auxiliaray
    }
}
