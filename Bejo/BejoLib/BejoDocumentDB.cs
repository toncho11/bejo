﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BejoLib
{
    public static class BejoDocumentDB
    {
        static List<BejoDocument> Documents;

        static BejoDocumentDB()
        {
            Documents = new List<BejoDocument>();

            BejoDocument t1 = new BejoDocument();
            t1.ID = 1;
            t1.WrongText = "Je pense que elle viendra.";
            t1.TrueText = "Je pense qu'elle viendra.";
            Documents.Add(t1);

            BejoDocument t2 = new BejoDocument();
            t2.ID = 2;
            t2.WrongText = "Je pense que il viendra.";
            t2.TrueText = "Je pense qu'il viendra.";
            Documents.Add(t2);

            BejoDocument t3 = new BejoDocument();
            t3.ID = 3;
            t3.WrongText = "Je pense que on viendra.";
            t3.TrueText = "Je pense qu'on viendra.";
            Documents.Add(t3);

            BejoDocument t4 = new BejoDocument();
            t4.ID = 4;
            t4.WrongText = "Cette femme ? Je la aime !";
            t4.TrueText = "Cette femme ? Je l'aime !";
            Documents.Add(t4);

            BejoDocument t5 = new BejoDocument();
            t5.ID = 5;
            t5.WrongText = "Je ne sais pas si il viendra demain.";
            t5.TrueText = "Je ne sais pas s'il viendra demain.";
            Documents.Add(t5);


            BejoDocument t6 = new BejoDocument();
            t6.ID = 6;
            t6.WrongText = "C'est de le problème.";
            t6.TrueText = "C'est du problème.";
            Documents.Add(t6);


            BejoDocument t7 = new BejoDocument();
            t7.ID = 7;
            t7.WrongText = "C'est le acteur.";
            t7.TrueText = "C'est l'acteur.";
            Documents.Add(t7);

            BejoDocument t8 = new BejoDocument();
            t8.ID = 8;
            t8.WrongText = "Je suis un grand fille.";
            t8.TrueText = "Je suis une grande fille.";
            Documents.Add(t8);

            BejoDocument t9 = new BejoDocument();
            t9.ID = 9;
            t9.WrongText = "Le discussion va être court.";
            t9.TrueText = "La discussion va être courte.";
            t9.IsExtendedMode = true;
            Documents.Add(t9);

            BejoDocument t10 = new BejoDocument();
            t10.ID = 10;
            t10.WrongText = "Le petit pomme est dans la panier.";
            t10.TrueText = "La petite pomme est dans le panier.";
            Documents.Add(t10);

            BejoDocument t11 = new BejoDocument();
            t11.ID = 11;
            t11.WrongText = "Il y a un grand probabilité.";
            t11.TrueText = "Il y a une grande probabilité.";
            Documents.Add(t11);

            BejoDocument t12 = new BejoDocument();
            t12.ID = 12;
            t12.WrongText = "Je ai un problèmes.";
            t12.TrueText = "J'ai des problèmes.";
            Documents.Add(t12);

            BejoDocument t13 = new BejoDocument();
            t13.ID = 13;
            t13.WrongText = "Je ai vérifié le prises électrique.";
            t13.TrueText = "J'ai vérifié les prises électriques.";
            Documents.Add(t13);

            BejoDocument t14 = new BejoDocument();
            t14.ID = 14;
            t14.WrongText = "J'ai mis dans le sacs petit."; //to have an adjective after noun
            t14.TrueText = "J'ai mis dans les sacs petits.";
            Documents.Add(t14);

            BejoDocument t15 = new BejoDocument();
            t15.ID = 15;
            t15.WrongText = "Je apprends un grammaire lourd.";
            t15.TrueText = "J'apprends une grammaire lourde.";
            Documents.Add(t15);

            BejoDocument t16 = new BejoDocument();
            t16.ID = 16;
            t16.WrongText = "Je achète une sac lourde.";
            t16.TrueText = "J'achète un sac lourd.";
            Documents.Add(t16);

            BejoDocument t17 = new BejoDocument();
            t17.ID = 17;
            t17.WrongText = "Debian est une système d'exploitation libre pour votre ordinateur.";
            t17.TrueText = "Debian est un système d'exploitation libre pour votre ordinateur.";
            Documents.Add(t17);

            BejoDocument t18 = new BejoDocument();
            t18.ID = 18;
            t18.WrongText = "Une système d'exploitation est la suite des programmes de base et des utilitaires qui permettent à une ordinateur de fonctionner.";
            t18.TrueText = "Un système d'exploitation est la suite des programmes de base et des utilitaires qui permettent à un ordinateur de fonctionner.";
            Documents.Add(t18);

            BejoDocument t19 = new BejoDocument();
            t19.ID = 19;
            t19.WrongText = "C'est une bonne système.";
            t19.TrueText = "C'est un bon système.";
            Documents.Add(t19);

            BejoDocument t20 = new BejoDocument();
            t20.ID = 20;
            t20.WrongText = "Il est une système jolie.";
            t20.TrueText = "Il est un système joli.";
            Documents.Add(t20);

            BejoDocument t21 = new BejoDocument();
            t21.ID = 21;
            t21.WrongText = "La service informatique a créé la infrastructure pour ajouter plus des disques.";
            t21.TrueText = "Le service informatique a créé l'infrastructure pour ajouter plus des disques.";
            Documents.Add(t21);

            BejoDocument t22 = new BejoDocument();
            t22.ID = 22;
            t22.WrongText = "Qui est-ce qui a acheté ça?";
            t22.TrueText = "Qui est-ce qui a acheté ça?";
            Documents.Add(t22);

            BejoDocument t23 = new BejoDocument();
            t23.ID = 23;
            t23.WrongText = "Par contre la ancienne baie est disponible.";
            t23.TrueText = "Par contre l'ancienne baie est disponible.";
            Documents.Add(t23);

            BejoDocument t24 = new BejoDocument();
            t24.ID = 24;
            t24.WrongText = "Le femme est épuisé.";
            t24.TrueText = "La femme est épuisée.";
            t24.IsExtendedMode = true;
            Documents.Add(t24);

            BejoDocument t25 = new BejoDocument();
            t25.ID = 25;
            t25.WrongText = "C'est pour stocker le différents versions.";
            t25.TrueText = "C'est pour stocker les différentes versions.";
            Documents.Add(t25);

            BejoDocument t26 = new BejoDocument();
            t26.ID = 26;
            t26.WrongText = "Quand je etais jeune je ai fait des bêtises.";
            t26.TrueText = "Quand j'etais jeune j'ai fait des bêtises.";
            Documents.Add(t26);

            BejoDocument t27 = new BejoDocument();
            t27.ID = 27;
            t27.WrongText = "Lorsque elle viendra, je irai ouvrir la porte.";
            t27.TrueText = "Lorsqu'elle viendra, j'irai ouvrir la porte.";
            Documents.Add(t27);

            BejoDocument t28 = new BejoDocument();
            t28.ID = 28;
            t28.WrongText = "Ça explose!";
            t28.TrueText = "Ça explose!";
            Documents.Add(t28);

            BejoDocument t29 = new BejoDocument();
            t29.ID = 29;
            t29.WrongText = "France a un nouveau président.";
            t29.TrueText = "France a un nouveau président.";
            Documents.Add(t29);

            BejoDocument t30 = new BejoDocument();
            t30.ID = 30;
            t30.WrongText = "L'ex-Premier ministre a insisté sur le fait qu'une action militaire contre les trafiquants ne fonctionnerait pas.";
            t30.TrueText = "L'ex-Premier ministre a insisté sur le fait qu'une action militaire contre les trafiquants ne fonctionnerait pas.";
            Documents.Add(t30);

            BejoDocument t31 = new BejoDocument();
            t31.ID = 31;
            t31.WrongText = "L'ancien Premier ministre Alexis Tsipras a démissionné.";
            t31.TrueText = "L'ancien Premier ministre Alexis Tsipras a démissionné.";
            Documents.Add(t31);

            BejoDocument t32 = new BejoDocument();
            t32.ID = 32;
            t32.WrongText = "Il a démissionné le mois dernier.";
            t32.TrueText = "Il a démissionné le mois dernier.";
            Documents.Add(t32);

            BejoDocument t33 = new BejoDocument();
            t33.ID = 33;
            t33.WrongText = "Cela vient de eux.";
            t33.TrueText = "Cela vient d'eux.";
            Documents.Add(t33);

            BejoDocument t34 = new BejoDocument();
            t34.ID = 34;
            t34.WrongText = "Cela se passe entre eux.";
            t34.TrueText = "Cela se passe entre eux.";
            Documents.Add(t34);

            BejoDocument t35 = new BejoDocument();
            t35.ID = 35;
            t35.WrongText = "Mon coeur est le vôtre.";
            t35.TrueText = "Mon cœur est le vôtre.";
            Documents.Add(t35);

            BejoDocument t36 = new BejoDocument();
            t36.ID = 36;
            t36.WrongText = "Votre soeur est très belle.";
            t36.TrueText = "Votre sœur est très belle.";
            Documents.Add(t36);

            BejoDocument t37 = new BejoDocument();
            t37.ID = 37;
            t37.WrongText = "C'est mon faute.";
            t37.TrueText = "C'est ma faute.";
            Documents.Add(t37);

            BejoDocument t38 = new BejoDocument();
            t38.ID = 38;
            t38.WrongText = "C'est votre ordinateur?";
            t38.TrueText = "C'est votre ordinateur?";
            Documents.Add(t38);

            BejoDocument t39 = new BejoDocument();
            t39.ID = 39;
            t39.WrongText = "Le dossier est rangé dans la armoire.";
            t39.TrueText = "Le dossier est rangé dans l'armoire.";
            Documents.Add(t39);

            BejoDocument t40 = new BejoDocument();
            t40.ID = 40;
            t40.WrongText = "Il sait que au bout il y a une récompense.";
            t40.TrueText = "Il sait qu'au bout il y a une récompense.";
            Documents.Add(t40);

            BejoDocument t41 = new BejoDocument();
            t41.ID = 41;
            t41.WrongText = "Je ne sais pas si il est parti.";
            t41.TrueText = "Je ne sais pas s'il est parti.";
            Documents.Add(t41);

            BejoDocument t42 = new BejoDocument();
            t42.ID = 42;
            t42.WrongText = "Il se battra jusque au bout pour obtenir gain de cause.";
            t42.TrueText = "Il se battra jusqu'au bout pour obtenir gain de cause.";
            Documents.Add(t42);

            BejoDocument t43 = new BejoDocument();
            t43.ID = 43;
            t43.WrongText = "Je allais partir lorsque enfin il est apparu.";
            t43.TrueText = "J'allais partir lorsqu'enfin il est apparu.";
            Documents.Add(t43);

            BejoDocument t44 = new BejoDocument();
            t44.ID = 44;
            t44.WrongText = "Il est arrivé presque au bout de son travail.";
            t44.TrueText = "Il est arrivé presque au bout de son travail.";
            Documents.Add(t44);

            BejoDocument t45 = new BejoDocument();
            t45.ID = 45;
            t45.WrongText = "Il eut soudain quelque envie de tout quitter.";
            t45.TrueText = "Il eut soudain quelque envie de tout quitter.";
            Documents.Add(t45);

            BejoDocument t46 = new BejoDocument();
            t46.ID = 46;
            t46.WrongText = "Elle a été retardée par un quelconque incident.";
            t46.TrueText = "Elle a été retardée par un quelconque incident.";
            Documents.Add(t46);

            BejoDocument t47 = new BejoDocument();
            t47.ID = 47;
            t47.WrongText = "Il se sentait motivé parce que au bout il y avait une récompense.";
            t47.TrueText = "Il se sentait motivé parce qu'au bout il y avait une récompense.";
            Documents.Add(t47);

            BejoDocument t48 = new BejoDocument();
            t48.ID = 48;
            t48.WrongText = "C'est bien que au bout il y avait une récompense.";
            t48.TrueText = "C'est bien qu'au bout il y avait une récompense.";
            Documents.Add(t48);

            BejoDocument t49 = new BejoDocument();
            t49.ID = 49;
            t49.WrongText = "Ce est bien passé.";
            t49.TrueText = "C'est bien passé.";
            Documents.Add(t49);

            BejoDocument t50 = new BejoDocument();
            t50.ID = 50;
            t50.WrongText = "C'est de la folie!";
            t50.TrueText = "C'est de la folie!";
            Documents.Add(t50);

            BejoDocument t51 = new BejoDocument();
            t51.ID = 51;
            t51.WrongText = "C'est ce qui explique la formation des vulcanos.";
            t51.TrueText = "C'est ce qui explique la formation des vulcanos.";
            Documents.Add(t51);

            BejoDocument t52 = new BejoDocument();
            t52.ID = 52;
            t52.WrongText = "Les armées alliées sont venues sauver le pays jusque alors occupé par l'ennemi.";
            t52.TrueText = "Les armées alliées sont venues sauver le pays jusqu'alors occupé par l'ennemi.";
            Documents.Add(t52);

            BejoDocument t53 = new BejoDocument();
            t53.ID = 53;
            t53.WrongText = "C'est ma bon père.";
            t53.TrueText = "C'est mon bon père.";
            Documents.Add(t53);


            BejoDocument t54 = new BejoDocument();
            t54.ID = 54;
            t54.WrongText = "À ma arrivée j'ai vu son mail.";
            t54.TrueText = "À mon arrivée j'ai vu son mail.";
            Documents.Add(t54);

            BejoDocument t55 = new BejoDocument();
            t55.ID = 55;
            t55.WrongText = "C'est ma adresse.";
            t55.TrueText = "C'est mon adresse.";
            Documents.Add(t55);

            BejoDocument t56 = new BejoDocument();
            t56.ID = 56;
            t56.WrongText = "C'est évolution de anciennes plates-formes.";
            t56.TrueText = "C'est évolution d'anciennes plates-formes.";
            Documents.Add(t56);

            BejoDocument t57 = new BejoDocument();
            t57.ID = 57;
            t57.WrongText = "Jusque en 2012, nous maintenons ce programme.";
            t57.TrueText = "Jusqu'en 2012, nous maintenons ce programme.";
            Documents.Add(t57);

            BejoDocument t58 = new BejoDocument();
            t58.ID = 58;
            t58.WrongText = "C'est ta amie.";
            t58.TrueText = "C'est ton amie.";
            Documents.Add(t58);

            BejoDocument t59 = new BejoDocument();
            t59.ID = 59;
            t59.WrongText = "Ce n'est pas ta problème.";
            t59.TrueText = "Ce n'est pas ton problème.";
            Documents.Add(t59);

            BejoDocument t60 = new BejoDocument();
            t60.ID = 60;
            t60.WrongText = "Ce n'est pas ton tasse.";
            t60.TrueText = "Ce n'est pas ta tasse.";
            Documents.Add(t60);

            BejoDocument t61 = new BejoDocument();
            t61.ID = 61;
            t61.WrongText = "C'est ton choix.";
            t61.TrueText = "C'est ton choix.";
            Documents.Add(t61);

            BejoDocument t62 = new BejoDocument();
            t62.ID = 62;
            t62.WrongText = "C'est mon collègue.";
            t62.TrueText = "C'est mon collègue.";
            Documents.Add(t62);

            BejoDocument t63 = new BejoDocument();
            t63.ID = 63;
            t63.WrongText = "C'est sa problème.";
            t63.TrueText = "C'est son problème.";
            Documents.Add(t63);

            BejoDocument t64 = new BejoDocument();
            t64.ID = 64;
            t64.WrongText = "C'est sa amie.";
            t64.TrueText = "C'est son amie.";
            Documents.Add(t64);

            BejoDocument t65 = new BejoDocument();
            t65.ID = 65;
            t65.WrongText = "J'ai apporté mon enfants.";
            t65.TrueText = "J'ai apporté mes enfants.";
            Documents.Add(t65);

            BejoDocument t66 = new BejoDocument();
            t66.ID = 66;
            t66.WrongText = "Je ne sais pas ta amis.";
            t66.TrueText = "Je ne sais pas tes amis.";
            Documents.Add(t66);

            BejoDocument t67 = new BejoDocument();
            t67.ID = 67;
            t67.WrongText = "Seules ses actions dévoileront sa réelles intentions.";
            t67.TrueText = "Seules ses actions dévoileront ses réelles intentions.";
            Documents.Add(t67);

            BejoDocument t68 = new BejoDocument();
            t68.ID = 68;
            t68.WrongText = "Il dévorera sa parents et son frères si nécessaire.";
            t68.TrueText = "Il dévorera ses parents et ses frères si nécessaire.";
            Documents.Add(t68);

            BejoDocument t69 = new BejoDocument();
            t69.ID = 69;
            t69.WrongText = "Une occasion d'explorer ses besoins, ses intérêts, ses valeurs et ses aptitudes.";
            t69.TrueText = "Une occasion d'explorer ses besoins, ses intérêts, ses valeurs et ses aptitudes.";
            Documents.Add(t69);

            BejoDocument t70 = new BejoDocument();
            t70.ID = 70;
            t70.WrongText = "La vitesse du son est de 340 m / s.Il n'y a pas de son.";
            t70.TrueText = "La vitesse du son est de 340 m / s.Il n'y a pas de son.";
            Documents.Add(t70);

            BejoDocument t71 = new BejoDocument();
            t71.ID = 71;
            t71.WrongText = "Dans mon jolie école.";
            t71.TrueText = "Dans ma jolie école.";
            Documents.Add(t71);

            BejoDocument t72 = new BejoDocument();
            t72.ID = 72;
            t72.WrongText = "Je voyais ma cher amis.";
            t72.TrueText = "Je voyais mes chers amis.";
            Documents.Add(t72);

            BejoDocument t73 = new BejoDocument();
            t73.ID = 73;
            t73.WrongText = "Elle fait confiance à sa meilleur ami.";
            t73.TrueText = "Elle fait confiance à son meilleur ami.";
            Documents.Add(t73);

            BejoDocument t74 = new BejoDocument();
            t74.ID = 74;
            t74.WrongText = "Il est situé dans cette bâtiment.";
            t74.TrueText = "Il est situé dans ce bâtiment.";
            Documents.Add(t74);

            BejoDocument t75 = new BejoDocument();
            t75.ID = 75;
            t75.WrongText = "Je vais manger cette pomme.";
            t75.TrueText =  "Je vais manger cette pomme.";
            Documents.Add(t75);

            BejoDocument t76 = new BejoDocument();
            t76.ID = 76;
            t76.WrongText = "Je ne vais pas répondre à ce question.";
            t76.TrueText = "Je ne vais pas répondre à cette question.";
            Documents.Add(t76);

            BejoDocument t77 = new BejoDocument();
            t77.ID = 77;
            t77.WrongText = "Il est chirurgien dans ce hôpital.";
            t77.TrueText = "Il est chirurgien dans cet hôpital.";
            Documents.Add(t77);

            BejoDocument t78 = new BejoDocument();
            t78.ID = 78;
            t78.WrongText = "Le jeu sortira cette hiver sur Playstation 3.";
            t78.TrueText = "Le jeu sortira cet hiver sur Playstation 3.";
            Documents.Add(t78);

            BejoDocument t79 = new BejoDocument();
            t79.ID = 79;
            t79.WrongText = "Je ne veux pas parler de cette problème.";
            t79.TrueText = "Je ne veux pas parler de ce problème.";
            Documents.Add(t79);

            BejoDocument t80 = new BejoDocument();
            t80.ID = 80;
            t80.WrongText = "Elle cache trop votre belle visage.";
            t80.TrueText = "Elle cache trop votre beau visage.";
            Documents.Add(t80);

            BejoDocument t81 = new BejoDocument();
            t81.ID = 81;
            t81.WrongText = "Vous êtes un bel spécimen humain.";
            t81.TrueText =  "Vous êtes un beau spécimen humain.";
            Documents.Add(t81);

            BejoDocument t82 = new BejoDocument();
            t82.ID = 82;
            t82.WrongText = "C'était un belle endroit, le maire voulait une chose spectaculaire.";
            t82.TrueText = "C'était un bel endroit, le maire voulait une chose spectaculaire.";
            Documents.Add(t82);

            BejoDocument t83 = new BejoDocument();
            t83.ID = 83;
            t83.WrongText = "Julianna est agent immobilier dans le vieux quartier.";
            t83.TrueText = "Julianna est agent immobilier dans le vieux quartier.";
            Documents.Add(t83);

            BejoDocument t84 = new BejoDocument();
            t84.ID = 84;
            t84.WrongText = "On dirait une vieil installation militaire.";
            t84.TrueText = "On dirait une vieille installation militaire.";
            Documents.Add(t84);

            BejoDocument t85 = new BejoDocument();
            t85.ID = 85;
            t85.WrongText = "C'est une vieille tradition américaine.";
            t85.TrueText = "C'est une vieille tradition américaine.";
            Documents.Add(t85);

            BejoDocument t86 = new BejoDocument();
            t86.ID = 86;
            t86.WrongText = "Nous sommes attaqués par un vieille ennemi.";
            t86.TrueText = "Nous sommes attaqués par un vieil ennemi.";
            Documents.Add(t86);

            BejoDocument t87 = new BejoDocument();
            t87.ID = 87;
            t87.WrongText = "On fera l'échange au vieille entrepôt.";
            t87.TrueText = "On fera l'échange au vieil entrepôt."; //entrepôt m.
            Documents.Add(t87);

            BejoDocument t88 = new BejoDocument();
            t88.ID = 88;
            t88.WrongText = "C'est une herbe vieil.";
            t88.TrueText = "C'est une herbe vieille."; 
            Documents.Add(t88);

            BejoDocument t89 = new BejoDocument();
            t89.ID = 89;
            t89.WrongText = "Ce question est stupide.";
            t89.TrueText = "Cette question est stupide."; 
            Documents.Add(t89);

            BejoDocument t90 = new BejoDocument();
            t90.ID = 90;
            t90.WrongText = "Pour que on vous suive, montrez votre détermination.";
            t90.TrueText = "Pour qu'on vous suive, montrez votre détermination."; 
            Documents.Add(t90);

            BejoDocument t91 = new BejoDocument();
            t91.ID = 91;
            t91.WrongText = "Ce est une probleme.";
            t91.TrueText = "C'est un problème."; 
            Documents.Add(t91);

            BejoDocument t92 = new BejoDocument();
            t92.ID = 92;
            t92.WrongText = "Il est etudiant.";
            t92.TrueText = "Il est étudiant.";
            Documents.Add(t92);

            BejoDocument t93 = new BejoDocument();
            t93.ID = 93;
            t93.WrongText = "La reseau ne marche pas.";
            t93.TrueText = "Le réseau ne marche pas.";
            Documents.Add(t93);

            BejoDocument t94 = new BejoDocument();
            t94.ID = 94;
            t94.WrongText = "Les crevettes représentent un autre debouche pour les exportateurs canadiens.";
            t94.TrueText =  "Les crevettes représentent un autre débouché pour les exportateurs canadiens.";
            Documents.Add(t94);

            BejoDocument t95 = new BejoDocument();
            t95.ID = 95;
            t95.WrongText = "C'est une bonne analyse.";
            t95.TrueText = "C'est une bonne analyse.";
            Documents.Add(t95);

            BejoDocument t96 = new BejoDocument();
            t96.ID = 96;
            t96.WrongText = "L'egalite devant la loi est un principe constitutionnel.";
            t96.TrueText = "L'égalité devant la loi est un principe constitutionnel.";
            Documents.Add(t96);

            BejoDocument t97 = new BejoDocument();
            t97.ID = 97;
            t97.WrongText = "Alexis Tsipras a remporté son pari dimanche soir. Pour la troisième fois en huit mois, les Grecs lui ont renouvelé leur confiance. Alexis Tsipras devrait maintenant former une coalition avec la droite souverainiste, en éliminant l'aile gauche de son parti. Il devrait ainsi disposer des 155 sièges nécessaires au Parlement pour mettre en œuvre le difficile troisième plan d'austérité accepté en juillet.";
            t97.TrueText = "Alexis Tsipras a remporté son pari dimanche soir. Pour la troisième fois en huit mois, les Grecs lui ont renouvelé leur confiance. Alexis Tsipras devrait maintenant former une coalition avec la droite souverainiste, en éliminant l'aile gauche de son parti. Il devrait ainsi disposer des 155 sièges nécessaires au Parlement pour mettre en œuvre le difficile troisième plan d'austérité accepté en juillet.";
            Documents.Add(t97);

            BejoDocument t98 = new BejoDocument();
            t98.ID = 98;
            t98.WrongText = "Deezer a annoncé, mardi, avoir entamé le processus d'entree en Bourse.";
            t98.TrueText = "Deezer a annoncé, mardi, avoir entamé le processus d'entrée en Bourse.";
            Documents.Add(t98);

            BejoDocument t99 = new BejoDocument();
            t99.ID = 99;
            t99.WrongText = "L'introduction en Bourse devrait rapporter à Deezer les fonds necessaires pour aller à la pêche aux nouveaux utilisateurs.";
            t99.TrueText =  "L'introduction en Bourse devrait rapporter à Deezer les fonds nécessaires pour aller à la pêche aux nouveaux utilisateurs.";
            Documents.Add(t99);

            BejoDocument t100 = new BejoDocument();
            t100.ID = 100;
            t100.WrongText = "La marque à la pomme à des moyens financiers pour s'imposer dont la societe française ne peut que rêver.";
            t100.TrueText =  "La marque à la pomme à des moyens financiers pour s'imposer dont la société française ne peut que rêver.";
            Documents.Add(t100);

            BejoDocument t101 = new BejoDocument();
            t101.ID = 101;
            t101.WrongText = "La marque à la pomme à des moyens financiers pour s'imposer dont la sociéte française ne peut que rêver.";
            t101.TrueText =  "La marque à la pomme à des moyens financiers pour s'imposer dont la société française ne peut que rêver.";
            Documents.Add(t101);

            BejoDocument t102 = new BejoDocument();
            t102.ID = 102;
            t102.WrongText = "Viens voir ma nouvel voiture.";
            t102.TrueText = "Viens voir ma nouvelle voiture.";
            Documents.Add(t102);

            BejoDocument t103 = new BejoDocument();
            t103.ID = 103;
            t103.WrongText = "Il vient d'emménager dans son nouveau appartement.";
            t103.TrueText  = "Il vient d'emménager dans son nouvel appartement.";
            Documents.Add(t103);

            BejoDocument t104 = new BejoDocument();
            t104.ID = 104;
            t104.WrongText = "Il faut un nouvel lycee.";
            t104.TrueText  = "Il faut un nouveau lycée.";
            Documents.Add(t104);

            BejoDocument t105 = new BejoDocument();
            t105.ID = 105;
            t105.WrongText = "Dans les années 60, on a créé des villes nouveaux.";
            t105.TrueText = "Dans les années 60, on a créé des villes nouvelles.";
            Documents.Add(t105);

            BejoDocument t106 = new BejoDocument();
            t106.ID = 106;
            t106.WrongText = "Il y a un nouvel élève dans ma classe.";
            t106.TrueText  = "Il y a un nouvel élève dans ma classe.";
            Documents.Add(t106);

            BejoDocument t107 = new BejoDocument();
            t107.ID = 107;
            t107.WrongText = "C'est un nouvel methode.";
            t107.TrueText  = "C'est une nouvelle méthode.";
            Documents.Add(t107);

            BejoDocument t108 = new BejoDocument();
            t108.ID = 108;
            t108.WrongText = "Je ne connais pas quelqu'un qui utilise le gaz."; //qui + utilise stays
            t108.TrueText  = "Je ne connais pas quelqu'un qui utilise le gaz.";
            Documents.Add(t108);

            BejoDocument t109 = new BejoDocument();
            t109.ID = 109;
            t109.WrongText = "Voulez-vous m'aider avec ces 2 problème?";
            t109.TrueText =  "Voulez-vous m'aider avec ces 2 problèmes?";
            Documents.Add(t109);

            BejoDocument t110 = new BejoDocument();
            t110.ID = 110;
            t110.WrongText = "Qu'est-ce que le departement?";
            t110.TrueText = "Qu'est-ce que le département?";
            Documents.Add(t110);

            BejoDocument t111 = new BejoDocument();
            t111.ID = 111;
            t111.WrongText = "Il est possible de écrire ses codes sources directement depuis le bloc-note de Windows.";
            t111.TrueText = "Il est possible d'écrire ses codes sources directement depuis le bloc-note de Windows.";
            Documents.Add(t111);

            BejoDocument t112 = new BejoDocument();
            t112.ID = 112;
            t112.WrongText = "Vous pouvez choisir de utiliser ces deux outils séparément.";
            t112.TrueText = "Vous pouvez choisir d'utiliser ces deux outils séparément.";
            Documents.Add(t112);

            BejoDocument t113 = new BejoDocument();
            t113.ID = 113;
            t113.WrongText = "J'ai trouvé un souris d’ordinateur.";
            t113.TrueText = "J'ai trouvé une souris d’ordinateur.";
            Documents.Add(t113);

            BejoDocument t114 = new BejoDocument();
            t114.ID = 114;
            t114.WrongText = "Ce est une jeu video.";
            t114.TrueText = "C'est un jeu vidéo.";
            Documents.Add(t114);

            BejoDocument t115 = new BejoDocument();
            t115.ID = 115;
            t115.WrongText = "Qui est-ce que elle a rencontré?";
            t115.TrueText = "Qui est-ce qu'elle a rencontré?";
            Documents.Add(t115);

            BejoDocument t116 = new BejoDocument();
            t116.ID = 116;
            t116.WrongText = "J'ai tout ce que il me faut.";
            t116.TrueText = "J'ai tout ce qu'il me faut.";
            Documents.Add(t116);

            BejoDocument t117 = new BejoDocument();
            t117.ID = 117;
            t117.WrongText = "Je suis desole.";
            t117.TrueText = "Je suis désolé.";
            Documents.Add(t117);

            BejoDocument t118 = new BejoDocument();
            t118.ID = 118;
            t118.WrongText = "C'est à quel heure la repas?";
            t118.TrueText = "C'est à quelle heure le repas?";
            Documents.Add(t118);

            BejoDocument t119 = new BejoDocument();
            t119.ID = 119;
            t119.WrongText = "La histoire est à la fois l’étude et l'écriture des faits et des événements.";
            t119.TrueText = "L'histoire est à la fois l’étude et l'écriture des faits et des événements.";
            Documents.Add(t119);

            BejoDocument t120 = new BejoDocument();
            t120.ID = 120;
            t120.WrongText = "La maison est blanc.";
            t120.TrueText = "La maison est blanche.";
            Documents.Add(t120);

            BejoDocument t121 = new BejoDocument();
            t121.ID = 121;
            t121.WrongText = "En France, le ecole est obligatoire à partir de six ans. Avant ce âge, le grande majorité des enfants sont placés dès trois ans dans un école maternelle. A six ans, le enfant entre dans un école primaire, il y apprend à lire et à écrire en classe préparatoire (CP). Durant les quatre annees suivantes de école primaire, le enfant apprend les bases de nombreux matieres telles que le mathématiques, le histoire, ou encore le géographie.";
            t121.TrueText = "En France, l'école est obligatoire à partir de six ans. Avant cet âge, la grande majorité des enfants sont placés dès trois ans dans une école maternelle. A six ans, l'enfant entre dans une école primaire, il y apprend à lire et à écrire en classe préparatoire (CP). Durant les quatre années suivantes d'école primaire, l'enfant apprend les bases de nombreuses matières telles que les mathématiques, l'histoire, ou encore la géographie.";
            Documents.Add(t121);

            BejoDocument t122 = new BejoDocument();
            t122.ID = 122;
            t122.WrongText = "N'avez-vous donc plus des espoir?";
            t122.TrueText = "N'avez-vous donc plus de espoir?";
            Documents.Add(t122);

            BejoDocument t123 = new BejoDocument();
            t123.ID = 123;
            t123.WrongText = "Je préfère la premiere contact par e-mail.";
            t123.TrueText = "Je préfère le premier contact par e-mail.";
            Documents.Add(t123);

            BejoDocument t124 = new BejoDocument();
            t124.ID = 124;
            t124.WrongText = "Je préfère la premiere contact par e-mail.";
            t124.TrueText = "Je préfère le premier contact par e-mail.";
            Documents.Add(t124);

        }

        public static BejoDocument GetDocument(int i)
        {
            return Documents[i];
        }

        public static BejoDocument GetDocumentByID(int id)
        {
            return Documents.SingleOrDefault(x => x.ID == id);
        }

        public static BejoDocument[] GetAllDocuments()
        {
            return Documents.ToArray();
        }

        public static int GetDocumentsCount()
        {
            return Documents.Count;
        }

        public static int[] GetAbreviationRules()
        {
            return new int[] {1,2,3,4,5,7,12,15,23,26,28,29,30,31,33,34,38,39,40,41,42,43,44,45,46,47,48};
        }

        public static string GetDocumentDemo()
        {
            return GetDocumentByID(121).WrongText;
        }
    }

    public struct BejoDocument
    {
        public int ID;
        public string WrongText;
        public string TrueText;
        public bool IsExtendedMode;
    }
}
