﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

//corenlp
using edu.stanford.nlp.pipeline;
using Console = System.Console;
using edu.stanford.nlp.trees;
using edu.stanford.nlp.process;
using edu.stanford.nlp.parser.lexparser;

using System.Globalization;
using System.Threading;

using edu.stanford.nlp.parser.common;
using edu.stanford.nlp.parser.shiftreduce;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime;
using System.Runtime.CompilerServices;

namespace BejoLib
{
    /// <summary>
    /// Provides a tree that contains the structure of the sentence. 
    /// </summary>
    public class Parser
    {
        private static ParserGrammar bejoParser;
        private static bool isInitialized = false;

        private static bool IsBeamShiftReduced = true;

        static Parser()
        {
           Initialize();
        }

        public static void Initialize()
        {
            if (!isInitialized)
            {
                var modelsDirectory = Helper.GetNLPModelsFolder();

                //changing culture to avoid java exceptions
                CultureInfo ci = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;

                string modelLocation = "";

                if (IsBeamShiftReduced)
                {
                    //the only available shitf-reduced model for French is the beam one
                    modelLocation = modelsDirectory + System.IO.Path.DirectorySeparatorChar + "frenchSR.beam.ser.gz";
                    bejoParser = ShiftReduceParser.loadModel(modelLocation);
                }
                else
                {   //standard model 
                    modelLocation = modelsDirectory + System.IO.Path.DirectorySeparatorChar + "frenchFactored.ser.gz";
                    bejoParser = LexicalizedParser.loadModel(modelLocation);
                }

                isInitialized = true;
            }
        }

        /// <summary>
        /// This functionality is work in progress 
        /// </summary>
        public static string GetParsedText(string[] texts)
        {
            string result = "";

            if (bejoParser != null)
            {
                foreach (string text in texts)
                {
                    result = ProcessText(bejoParser, text);
                }
            }

            return result;
        }

        private static string ProcessText(ParserGrammar lp, string text)
        {
            string result = "";

            if (IsBeamShiftReduced)
            {
                var sentences = POS.GetTaggedSentences(text);

                foreach (List taggedSentence in sentences)
                {
                    Tree tree = lp.apply(taggedSentence);

                    result = tree.pennString();

                    //UD Universal Dependencies representation for French are not supported
                    Console.WriteLine(tree.pennString());
                }
            }
            else
            {
                var tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
                var sent2Reader = new java.io.StringReader(text);
                var rawWords2 = tokenizerFactory.getTokenizer(sent2Reader).tokenize();
                sent2Reader.close();
                var tree2 = lp.apply(rawWords2);

                result = tree2.pennString();

                //Universal Dependencies representation for French are not supported
                Console.WriteLine(tree2.pennString());
            }

            return result;
        }

        public static void SaveAsFile()
        {
            FileStream stream = System.IO.File.Create(@"d:\work\test.bin");
            var formatter = new BinaryFormatter();
            Console.WriteLine("Serializing...");
            formatter.Serialize(stream, bejoParser);
            stream.Close();
        }
    }
}
