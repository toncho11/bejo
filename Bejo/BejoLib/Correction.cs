﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

namespace BejoLib
{
    /// <summary>
    /// Applies all(or selected number of) rules to text 
    /// </summary>
    public static class Correction
    {
        static BejoCorrectionRule[] allRules;

        static BejoCorrectionRule[] basicWorkflowRules;

        static BejoCorrectionRule[] extendedWorkflowRules;

        static Correction()
        {
            allRules = new BejoCorrectionRule[]
                      { new RuleCorrectEs(),
                        new RuleCorrectDetForNouns(),
                        new RuleCorrectAdjectives(),
                        new RuleCorrectDetForNounsWithAdjectiveBetween(),
                        new RuleCorrectVerbDerivedAdj1(),
                        new RuleCorrectVerbDerivedAdj2(),
                        new RuleCorrectAbbreviations(),
                        new MergingPreDet(), //depends on RuleCorrectDetAbbreviation to be executed first
                        new RuleCorrectOdansE(),
                        new RuleCorrectAdjPosessif(),
                        new RuleCorrectAdjPosessifAdjectiveBetween(),
                      };

            //linq keeps the order - important as some rules must be executed before others
            basicWorkflowRules = allRules.Where(x => ((int)x.Staus) >= ((int)BejoImplementationRuleStatus.CoversMostCasesButNotAll)).ToArray();
            extendedWorkflowRules = allRules;
        }

        public delegate void TaskCompletedHandler(int completed);

        public static event TaskCompletedHandler TaskCompleted;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textOriginal"></param>
        /// <param name="correctionsPositions"></param>
        /// <returns></returns>
        public static BejoText Execute(string textOriginal, bool isBasicMode)
        {
            BejoCorrectionRule[] currentWorkflow;

            if (isBasicMode)
            {
                currentWorkflow = basicWorkflowRules;
            }
            else
            {
                currentWorkflow = extendedWorkflowRules;
            }

            List<List<BejoWord>> posTaggedSentences = POS.GetTaggedSentencesBejoWordFormat(textOriginal);

            BejoText btext = new BejoText(textOriginal, posTaggedSentences);

            if (TaskCompleted != null) TaskCompleted(20);

            int advancementPerRule = 75 / currentWorkflow.Length; //5% left for final

            int i = 1;
            foreach (var rule in currentWorkflow)
            {
                string correctedText = rule.Execute(ref btext);
                if (TaskCompleted != null) TaskCompleted(20 + (i * advancementPerRule));
                i++;
            }

            //StringBuilder bPosText = new StringBuilder("");
            //foreach (var sentence in posTaggedSentences)
            //{
            //    foreach (var word in sentence)
            //    {
            //        bPosText.Append(word.POSTaggedWord.value() + "/" + word.POSTaggedWord.tag() + " ");
            //    }
            //}
            //posText = bPosText.ToString();

            //Logging

            return btext;
        }

        public static BejoCorrectionRule[] GetBasicWorkflowRules
        {
            get
            {
                return basicWorkflowRules;
            }
        }

        public static BejoCorrectionRule[] GetExtendedWorkflowRules
        {
            get
            {
                return extendedWorkflowRules;
            }
        }
    }
}
