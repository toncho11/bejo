﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BejoLib;

using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Diagnostics;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for SelfTestWindow.xaml
    /// </summary>
    public partial class SelfTestWindow : Window
    {
        //http://stackoverflow.com/questions/493345/in-wpf-stretch-a-control-to-fill-a-listview-column

        BackgroundWorker worker;

        BejoTest[] Tests;

        bool IsBasicMode;

        Stopwatch stopwatch;

        public SelfTestWindow()
        {
            InitializeComponent();

            stopwatch = new Stopwatch();
            IsBasicMode = true;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;

            worker.DoWork += Worker_DoWork;
            //worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

            var tests = from d in BejoDocumentDB.GetAllDocuments().Where(x => IsBasicMode != x.IsExtendedMode)
                         select new DisplayTest { ID = d.ID, WrongText = d.WrongText, TrueText = d.TrueText, BejoCorrectedText = "", IsExtendedMode = d.IsExtendedMode, IsOK = "Not started", POStext = "" };

            lvTests.ItemsSource = tests;
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            stopwatch.Stop();
            if (e.Error != null)
            {
                MessageBox.Show("The following error has been detected: " + e.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                tbResult.Text = "Prcoessing has failed!";
            }
            else //all ok
            {
                DisplayTest[] displayTests = (
                                    from t in Tests

                                    //disable some tests
                                    where t.DocumentID!=52 && t.DocumentID !=96 && t.DocumentID !=98 && t.DocumentID !=99 && t.DocumentID !=109

                                    select new DisplayTest { ID = t.DocumentID, WrongText = t.WrongText, TrueText = t.TrueText, BejoCorrectedText = t.BejoCorrectedText, IsExtendedMode = t.IsExtendedMode, IsOK = (t.BejoCorrectedText.Equals(t.TrueText)) ? "OK" : "Failed", POStext = t.POStext }
                                    ).ToArray();

                lvTests.ItemsSource = displayTests;

                long elapsed_time = stopwatch.ElapsedMilliseconds;
                double seconds = ((double)elapsed_time) / 1000;
                tbResult.Text = "Tests failed: " + displayTests.Count(x => x.IsOK != "OK").ToString() + " / " + displayTests.Count().ToString() + ". Time elapsed in seconds: " +seconds.ToString("0.000");
            }

            buttonTesting.IsEnabled = true;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Tests = BejoTests.RunAllTests(IsBasicMode);
            //Tests = BejoTests.RunAbbreviationsTests(IsBasicMode);
        }

        private void buttonTesting_Click(object sender, RoutedEventArgs e)
        {
            buttonTesting.IsEnabled = false;

            tbResult.Text = "Processing, please wait ... ";
            stopwatch.Reset();
            stopwatch.Start();
            worker.RunWorkerAsync();
        }

        public class DisplayTest
        {
            public int ID { get; set; }

            public string WrongText { get; set; }

            public string TrueText { get; set; }

            public bool IsExtendedMode { get; set; }

            public string BejoCorrectedText { get; set; }

            public string IsOK { get; set; }

            public string POStext { get; set; }
        }

        private void buttonCopySelected_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.InputText = ((DisplayTest)lvTests.SelectedItem).WrongText;
        }
    }
}
