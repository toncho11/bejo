﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

using BejoLib;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for DictionaryWindow.xaml
    /// </summary>
    public partial class DictionaryWindow : Window
    {
        public DictionaryWindow()
        {
            InitializeComponent();

            textboxSearch.Focusable = true;
            textboxSearch.Focus();
        }

        private void buttonSearch_Click(object sender, RoutedEventArgs e)
        {
            string word = BejoNLPhelper.ConvertToLowerAllE(textboxSearch.Text);

            DisplayWord[] results = (from w in BejoDictionary.BejoWordDictionary.AsParallel()
                           let x = w.AllESame.ToLower()
                           where x == word
                           select new DisplayWord { Content = w.Content, Gender = w.Gender.ToString(), Number = w.Number.ToString(), Type=w.Type.ToString() }).ToArray();

            lvWords.ItemsSource = results;
        }

        private void textboxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buttonSearch_Click(null, null);
            }
        }
    }

    public class DisplayWord
    {
        public string Content { get; set; }

        public string Type { get; set; }

        public string Gender { get; set; }

        public string Number { get; set; }
    }

    


}
