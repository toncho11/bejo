﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for AboutForm.xaml
    /// </summary>
    public partial class AboutForm : Window
    {
        public AboutForm()
        {
            InitializeComponent();

            tbName.Text += " " + BejoLib.Helper.BejoVersion;
        }

        private void btAuthor_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://fr.linkedin.com/in/antonandreev");
        }

        private void btWebSite_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://sourceforge.net/projects/bejo"); 
        }

        private void btStandford_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://nlp.stanford.edu/software/tagger.shtml");
        }

        private void btLeFFF_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://alpage.inria.fr/~sagot/lefff-en.html");
        }
    }
}
