﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

using BejoLib;
using System.ComponentModel;

using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Automation.Text;
using System.Diagnostics;
using System.Runtime;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //string posText;
        BejoText btext;

        BackgroundWorker workerTextProcessor;

        BackgroundWorker workerLoader;

        bool isBasicMode;

        DictionaryWindow dw;

        RulesWindow rw;

        Stopwatch stopwatch;

        string DictFullPath;

        string parsedText;

        BejoOperation selectedOperation;

        public MainWindow()
        {
            InitializeComponent();

            
            //string defaultText = "C'est un nouvel methode.";//Ce question est stupide.
            //string defaultText = "Voulez-vous m'aider avec ces 2 problème?";
            //string defaultText = BejoDocumentDB.GetDocumentByID(76).WrongText;

            //InputText = defaultText;          

            stopwatch = new Stopwatch();

            workerTextProcessor = new BackgroundWorker();
            workerTextProcessor.WorkerReportsProgress = true;
            workerTextProcessor.DoWork += Worker_DoWork;
            workerTextProcessor.ProgressChanged += Worker_ProgressChanged;
            workerTextProcessor.RunWorkerCompleted += Worker_RunWorkerCompleted;

            workerLoader = new BackgroundWorker();
            workerLoader.WorkerReportsProgress = true;
            workerLoader.DoWork += WorkerLoader_DoWork;
            workerLoader.ProgressChanged += WorkerLoader_ProgressChanged;
            workerLoader.RunWorkerCompleted += WorkerLoader_RunWorkerCompleted;

            tbStaus.Text = "Loading ...";
            buttonProcess.IsEnabled = false;
            buttonTesting.IsEnabled = false;
            buttonDictionary.IsEnabled = false;

            DictFullPath = Helper.GetDefaultDictionaryPath();
            if (!System.IO.File.Exists(DictFullPath)) { System.Windows.MessageBox.Show("Could not load dictionary. Check if all files are available. Release and Debug modes search differently for the resource files.", "Error", MessageBoxButton.OK, MessageBoxImage.Error); }

            workerLoader.RunWorkerAsync();

            textBoxOutputText.PreviewMouseRightButtonDown += TextBoxOutputText_PreviewMouseRightButtonDown;
            BuildContextMenu(new string[] { });

            InputText = BejoLib.BejoDocumentDB.GetDocumentDemo();
        }

        private void WorkerLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            stopwatch.Stop();
            if (e.Error != null)
            {
                MessageBox.Show("The following error has been detected: " + e.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
            else
            {
                progressBar.Value = 100;

                long elapsed_time = stopwatch.ElapsedMilliseconds;
                double seconds = ((double)elapsed_time) / 1000;
                stopwatch.Reset();
                tbStaus.Text = "Ready. Start-up took " + seconds.ToString() + " seconds.";

                buttonProcess.IsEnabled = true;
                buttonTesting.IsEnabled = true;
                buttonDictionary.IsEnabled = true;
            }
        }

        private void WorkerLoader_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void WorkerLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            stopwatch.Start();

            //1. Load dictionary
            BejoDictionary.ImportLefff(DictFullPath);
            workerLoader.ReportProgress(20);

            POS.Initialize();
            workerLoader.ReportProgress(60);

            //Parser.Initialize();
        }

        private void TextBoxOutputText_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var document = textBoxOutputText.Document;
            var screenPoint = PointToScreen(e.GetPosition(this));

            TextPointer caretPos = document.ScreenPointToTextPointer(screenPoint);

            var before = caretPos.GetTextInRun(LogicalDirection.Backward);
            var after = caretPos.GetTextInRun(LogicalDirection.Forward);


            int t1 = before.LastIndexOf(" ");
            int t2 = after.IndexOf(" ");

            if (t1 == -1) t1 = 0;
            if (t2 == -1) t2 = after.Length;

            if (t1 == -1 || t2 == -1)
            {
                BuildContextMenu(new string[] { });
                return;
            }

            string beforePart = before.Substring(t1);
            string afterPart = after.Substring(0, t2);

            string selectedWord = beforePart + afterPart;

            bool match_found = false;

            if (!string.IsNullOrEmpty(selectedWord.Trim()) && btext != null && btext.TaggedWordsCount > 0)
            {
                BejoWord[] taggedWords = btext.GetTaggedWords;

                foreach (var taggedWord in taggedWords)
                {
                    if (taggedWord.IsCorrected)
                    {
                        int offset = taggedWord.CorrectionPosition;
                        int length = taggedWord.CorrectionLength;

                        var start = textBoxOutputText.Document.ContentStart;
                        var startPos = GetPoint(start, offset);

                        string text = new TextRange(startPos, GetPoint(startPos, length)).Text;

                        int p1 = textBoxOutputText.Document.ContentStart.GetOffsetToPosition(startPos);
                        int p2 = textBoxOutputText.Document.ContentStart.GetOffsetToPosition(caretPos) - beforePart.Length - 2;

                        if (text.StartsWith(" ")) p2 -= 2;

                        if (p1 == p2)
                        {
                            List<string> menuStrings = new List<string>();

                            if (taggedWord.CorrectionWords != null)
                            {
                                foreach (var correction in taggedWord.CorrectionWords)
                                {
                                    if (correction.Content == selectedWord)
                                        menuStrings.Add(correction.ToExtendedString + " (selected)");
                                    else menuStrings.Add(correction.ToExtendedString);
                                }
                            }

                            menuStrings.Add(taggedWord.OriginalWord + " (original)"); //add what was also the word in tge very beginning

                            BuildContextMenu(menuStrings.ToArray());

                            match_found = true;
                            break;


                        }
                    }
                }
            }

            if (!match_found)
                BuildContextMenu(new string[] { });
        }

        private void Correction_TaskCompleted(int completed)
        {
            workerTextProcessor.ReportProgress(completed);
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            stopwatch.Stop();
            if (e.Error != null)
            {
                MessageBox.Show("The following error has been detected: " + e.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (selectedOperation == BejoOperation.Correction)
                {
                    OutputText = btext.GetModifiedText();

                    BejoWord[] taggedWords = btext.GetTaggedWords;

                    //BejoWord[] invalidOnes = taggedWords.Where(x => x.CorrectionLength < 0).ToArray();

                    //Underline
                    foreach (var word in taggedWords)
                    {
                        if (word.IsCorrected)
                        {
                            Color color;

                            if (word.CorrectionWords != null && word.CorrectionWords.Length > 1)
                            {
                                color = Colors.OrangeRed; //more than one correction
                            }
                            else
                            {
                                color = Colors.Blue; //only one correction
                            }

                            //if (word.CorrectionLength > 0 )
                            Colorize(word.CorrectionPosition, word.CorrectionLength, color);
                        }
                    }
                }
                else
                if (selectedOperation == BejoOperation.Parser)
                {
                    OutputText = parsedText;
                }

                progressBar.Value = 100;

                long elapsed_time = stopwatch.ElapsedMilliseconds;
                double seconds = ((double)elapsed_time) / 1000;
                tbStaus.Text = "Done. Time elapsed in seconds: " + seconds.ToString("0.000");
            }

            BejoLib.Correction.TaskCompleted -= Correction_TaskCompleted;

            buttonProcess.IsEnabled = true;
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (selectedOperation == BejoOperation.Correction)
            {
                btext = Correction.Execute(InputText, isBasicMode);
            }
            else
            if (selectedOperation == BejoOperation.Parser)
            {
                parsedText = Parser.GetParsedText(new string[] { InputText });
            }
        }

        private void buttonProcess_Click(object sender, RoutedEventArgs e)
        {
            textBoxOutputText.Document.Blocks.Clear();

            isBasicMode = radioButtonBasicMode.IsChecked ?? false;

            if (radioButtonPOS.IsChecked ?? false)
                selectedOperation = BejoOperation.POS;
            else
            if (radioButtonCorrection.IsChecked ?? false)
                selectedOperation = BejoOperation.Correction;
            else
            if (radioButtonParser.IsChecked ?? false)
                selectedOperation = BejoOperation.Parser;


            if (selectedOperation == BejoOperation.POS)
            {
                progressBar.Value = 5;
                string posText = POS.GetTaggedText(InputText);
                OutputText = posText;
                progressBar.Value = 100;
            }
            else
            if (selectedOperation == BejoOperation.Correction || selectedOperation == BejoOperation.Parser)
            {
                stopwatch.Reset();
                stopwatch.Start();
                BejoLib.Correction.TaskCompleted += Correction_TaskCompleted;
                workerTextProcessor.RunWorkerAsync();
                buttonProcess.IsEnabled = false;
                progressBar.Value = 5;
            }

            tbStaus.Text = "Processing ... please wait. It might take longer for bigger texts.";
        }
        private void buttonExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        public string InputText
        {
            set
            {
                textBoxInputText.Document.Blocks.Clear();
                textBoxInputText.Document.Blocks.Add(new Paragraph(new Run(value)));
            }
            get
            {
                return new TextRange(textBoxInputText.Document.ContentStart, textBoxInputText.Document.ContentEnd).Text;
            }
        }

        public string OutputText
        {
            set
            {
                textBoxOutputText.Document.Blocks.Clear();
                textBoxOutputText.Document.Blocks.Add(new Paragraph(new Run(value)));
            }
        }

        private void Colorize(int offset, int length, Color color)
        {
            var textRange = textBoxOutputText.Selection;
            var start = textBoxOutputText.Document.ContentStart;
            var startPos = GetPoint(start, offset);
            var endPos = GetPoint(start, offset + length);

            textRange.Select(startPos, endPos);
            textRange.ApplyPropertyValue(TextElement.ForegroundProperty,
                new SolidColorBrush(color));
            textRange.ApplyPropertyValue(TextElement.FontWeightProperty,
                FontWeights.Bold);
        }

        private static TextPointer GetPoint(TextPointer start, int x)
        {
            var ret = start;
            var i = 0;
            while (i < x && ret != null)
            {
                if (ret.GetPointerContext(LogicalDirection.Backward) ==
                    TextPointerContext.Text ||
                                ret.GetPointerContext(LogicalDirection.Backward) ==
                    TextPointerContext.None)
                    i++;
                if (ret.GetPositionAtOffset(1,
        LogicalDirection.Forward) == null)
                    return ret;
                ret = ret.GetPositionAtOffset(1,
        LogicalDirection.Forward);
            }
            return ret;
        }

        private void buttonDictionary_Click(object sender, RoutedEventArgs e)
        {
            dw = new DictionaryWindow();
            dw.Show();
        }

        private void buttonRules_Click(object sender, RoutedEventArgs e)
        {
            rw = new RulesWindow();
            rw.Show();
        }

        private void buttonTesting_Click(object sender, RoutedEventArgs e)
        {
            SelfTestWindow sw = new SelfTestWindow();
            sw.Show();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            AboutForm af = new AboutForm();
            af.Show();
        }

        private void BuildContextMenu(string[] suggestions)
        {
            //http://www.a2zdotnet.com/View.aspx?Id=92#.VgK22NWqpBc
            //http://stackoverflow.com/questions/7582552/contextmenu-in-wpf

            ContextMenu mainMenu = new ContextMenu();

            if (suggestions.Length > 0)
            {
                foreach (var suggestion in suggestions)
                {
                    MenuItem item = new MenuItem();
                    item.Click += Item_Click;
                    mainMenu.Items.Add(item);
                    item.Header = suggestion;
                }
            }
            else
            {
                mainMenu.Width = 0;
                mainMenu.Height = 0;
            }

            textBoxOutputText.ContextMenu = mainMenu;
        }

        private void Item_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;

            //TextPointer caretPos = textBoxOutputText.CaretPosition;

            string word = (string)item.Header;
            tbStaus.Text = word;
        }

        //private void Window_MouseMove(object sender, MouseEventArgs e)
        //{
        //    var document = textBoxOutputText.Document;
        //    var screenPoint = PointToScreen(e.GetPosition(this));

        //    TextPointer pointer = document.ScreenPointToTextPointer(screenPoint);

        //    new TextRange(document.ContentStart, pointer).ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
        //    new TextRange(pointer, document.ContentEnd).ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Normal);
        //}
    }
}

public static class DocumentExtensions
{
    // Point is specified relative to the given visual
    public static TextPointer ScreenPointToTextPointer(this FlowDocument document, Point screenPoint)
    {
        // Get text before point using automation
        var peer = new DocumentAutomationPeer(document);
        var textProvider = (ITextProvider)peer.GetPattern(PatternInterface.Text);
        var rangeProvider = textProvider.RangeFromPoint(screenPoint);
        rangeProvider.MoveEndpointByUnit(TextPatternRangeEndpoint.Start, TextUnit.Document, 1);
        int charsBeforePoint = rangeProvider.GetText(int.MaxValue).Length;

        // Find the pointer that corresponds to the TextPointer
        var pointer = document.ContentStart.GetPositionAtOffset(charsBeforePoint);

        // Adjust for difference between "text offset" and actual number of characters before pointer
        for (int i = 0; i < 10; i++)  // Limit to 10 adjustments
        {
            int error = charsBeforePoint - new TextRange(document.ContentStart, pointer).Text.Length;
            if (error == 0) break;
            pointer = pointer.GetPositionAtOffset(error);
        }
        return pointer;
    }
}

enum BejoOperation
{
    POS,
    Parser,
    Correction
}
