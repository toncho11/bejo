# Bejo 

**Summary:** 
Bejo is a grammar checker for the French language. It can be very useful for foreigners writing e-mails in French. 

You can try the service online [here](http://http://bejo.micromedia.bg).

Bejo checks if you use the correct un/une/des/le/la/les, the correct gender and number of adjectives (based on noun), abbreviations such as: de + le => du, je ai => j'ai, que au => qu'au, le acteur => l'acteur, ta problème -> ton problème, etc. Conjugation of verbs is currently not supported.

Bejo uses a French dictionary and the [Stanford POS](http://nlp.stanford.edu/software/tagger.shtml) (Part of Speech Tagger). Bejo is written to be an extensible platform.

Bejo comes with more than 120 tests. If you find an error, please report back. It will be added as a test and corrected. 

Bejo can be used as web-site, desktop application or Android application. 

The Microsoft Windows version of Bejo has a better user interface compared to the one for Mac and Linux. The Mac/Linux version requires the latest version of [Mono](http://www.mono-project.com).


**Current focus:** searching for a way to utilize the Stanford Parser to find the relation between subject and verb in the sentence.